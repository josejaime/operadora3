<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ZoneController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\DepartureController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\ReservationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => 'auth'], function() {
    ///====================================================
    /// Users Routing
    ///====================================================
    Route::get('/users/create', [ UserController::class, 'create'])->name('users.create');
    Route::get('/users/reservations', [ UserController::class, 'reservations'])->name('users.reservations');
    Route::get('/users/{user}/reservations', [ UserController::class, 'userReservations'])->name('users.single.reservations');
    Route::get('/users', [ UserController::class, 'index' ])->name('users.index');
    Route::get('/users/{user}', [ UserController::class, 'show' ])->name('users.show');
    Route::get('/users/{user}/profile', [ UserController::class, 'profile' ])->name('users.profile');
    Route::put('/users/{user}/profile', [ UserController::class, 'updateProfile'])->name('users.update.profile');
    Route::put('/users/{user}', [UserController::class, 'update'])->name('users.update');
    Route::post('/users', [ UserController::class, 'store'])->name('users.store');
    Route::delete('/users/{user}', [ UserController::class, 'destroy'])->name('users.destroy');

    ///====================================================
    /// Roles Routing
    ///====================================================
    Route::get('/roles', [ RoleController::class, 'index'])->name('roles.index');
    Route::get('/roles/create', [ RoleController::class, 'create'])->name('roles.create');
    Route::post('/roles', [ RoleController::class, 'store'])->name('roles.store');
    Route::get('/roles/{role}', [ RoleController::class, 'show'])->name('roles.show');
    Route::get('/roles/{role}/edit', [ RoleController::class, 'edit'])->name('roles.edit');
    Route::put('/roles/{role}', [ RoleController::class, 'update'])->name('roles.update');
    Route::delete('/roles/{role}', [ RoleController::class, 'destroy'])->name('roles.destroy');

    ///====================================================
    /// Roles Routing
    ///====================================================
    Route::get('/zones', [ ZoneController::class, 'index'])->name('zones.index');
    Route::get('/zones/create', [ ZoneController::class, 'create'])->name('zones.create');
    Route::post('/zones', [ ZoneController::class, 'store'])->name('zones.store');
    Route::get('/zones/{zone}', [ ZoneController::class, 'show'])->name('zones.show');
    Route::get('/zones/{zone}/edit', [ ZoneController::class, 'edit'])->name('zones.edit');
    Route::put('/zones/{zone}', [ ZoneController::class, 'update'])->name('zones.update');
    Route::delete('/zones/{zone}', [ ZoneController::class, 'destroy'])->name('zones.destroy');

    ///====================================================
    /// Hotels Routing
    ///====================================================
    Route::get('/hotels', [ HotelController::class, 'index'])->name('hotels.index');
    Route::get('/hotels/create', [ HotelController::class, 'create'])->name('hotels.create');
    Route::post('/hotels', [ HotelController::class, 'store'])->name('hotels.store');
    Route::get('/hotels/{hotel}', [ HotelController::class, 'show'])->name('hotels.show');
    Route::get('/hotels/{hotel}/edit', [ HotelController::class, 'edit'])->name('hotels.edit');
    Route::put('/hotels/{hotel}', [ HotelController::class, 'update'])->name('hotels.update');
    Route::delete('/hotels/{hotel}', [ HotelController::class, 'destroy'])->name('hotels.destroy');

    ///====================================================
    /// Tours Routing
    ///====================================================
    Route::get('/tours', [ TourController::class, 'index'])->name('tours.index');
    Route::get('/tours/create', [ TourController::class, 'create'])->name('tours.create');
    Route::post('/tours', [ TourController::class, 'store'])->name('tours.store');
    Route::get('/tours/{tour}', [ TourController::class, 'show'])->name('tours.show');
    Route::get('/tours/{tour}/edit', [ TourController::class, 'edit'])->name('tours.edit');
    Route::put('/tours/{tour}', [ TourController::class, 'update'])->name('tours.update');
    Route::delete('/tours/{tour}', [ TourController::class, 'destroy'])->name('tours.destroy');

    ///====================================================
    /// Commissions Routing
    ///====================================================
    Route::get('/commissions/create/{user}', [ CommissionController::class, 'create'])->name('commissions.create');
    Route::post('/commissions', [ CommissionController::class, 'store'])->name('commissions.store');
    Route::get('/commissions/{commission}/edit', [ CommissionController::class, 'edit'])->name('commissions.edit');
    Route::put('/commissions/{commission}', [ CommissionController::class, 'update'])->name('commissions.update');
    Route::delete('/commissions/{commission}', [ CommissionController::class, 'destroy'])->name('commissions.destroy');

    ///====================================================
    /// Commissions Routing
    ///====================================================
    Route::get('/departures/create', [ DepartureController::class, 'create'])->name('departures.create');
    Route::post('/departures', [ DepartureController::class, 'store'])->name('departures.store');
    Route::get('/departures/{departure}/edit', [ DepartureController::class, 'edit'])->name('departures.edit');
    Route::put('/departures/{departure}', [ DepartureController::class, 'update'])->name('departures.update');
    Route::get('/departures/close', [ DepartureController::class, 'close'])->name('departures.close');
    Route::delete('/departures/{departure}', [ DepartureController::class, 'destroy'])->name('departures.destroy');

    ///====================================================
    /// Reservatiosn Routing
    ///====================================================
    Route::get('/reservations', [ ReservationController::class, 'index'])->name('reservations.index');
    Route::get('/reservations/select_tour', [ ReservationController::class, 'selectTour' ])->name('reservations.select.tour');
    Route::get('/reservations/create', [ ReservationController::class, 'create'])->name('reservations.create');
    Route::get('/reservations/create/select_seats', [ ReservationController::class, 'selectSeats' ])->name('reservations.create.select.seats');
    Route::post('/reservations', [ ReservationController::class, 'store'])->name('reservations.store');

    Route::get('/reservations/select_tour/review', [ ReservationController::class, 'selectTourBeforeReview' ])->name('reservations.select.tour.review');
    Route::get('/reservations/review_tour', [ ReservationController::class, 'reviewTour' ])->name('reservations.tour.review');
    Route::get('/reservations/{reservation}', [ ReservationController::class, 'show'])->name('reservations.show');
    Route::delete('/reservations/{reservation}', [ ReservationController::class, 'destroy'])->name('reservations.destroy');
});

Route::get('/pdf-test', function(){
    //$pdf = App::make('dompdf.wrapper');
    $pdf = PDF::loadView('pdfs.pdf_test');
    //$pdf->loadView('pdfs.pdf_test');
    return $pdf->stream();
});



require __DIR__.'/auth.php';
