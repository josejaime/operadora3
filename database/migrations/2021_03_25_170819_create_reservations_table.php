<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            //========================================
            // Reservation client info
            //========================================
            $table->tinyInteger('status')->default(0);
            $table->string('folio');
            $table->string('citypass')->nullable();
            $table->boolean('confirmed')->default(0);
            $table->string('client');
            $table->string('email')->nullable();
            $table->string('telephone')->nullable();
            $table->string('country')->nullable();
            $table->string('room')->nullable();
            $table->date('date');
            //========================================
            // Reservation common info
            //========================================
            $table->integer('kids')->default(0);
            $table->integer('adults')->default(0);
            $table->integer('elders')->default(0);
            //========================================
            // Reservation Prices info
            //========================================
            $table->decimal('price_kids',10,2)->default(0);
            $table->decimal('price_adults',10,2)->default(0);
            $table->decimal('price_elders',10,2)->default(0);
            $table->decimal('total',10,2);
            $table->decimal('remainig', 8,2)->default(0);
            //========================================
            // Reservation commission info
            //========================================
            $table->decimal('commission_kids',6,2)->nullable();
            $table->decimal('commission_adults',6,2)->nullable();
            $table->decimal('commission_elders',6,2)->nullable();
            $table->decimal('total_commission',6,2)->nullable();
            //========================================
            // Foreigns IDs
            //========================================
            $table->foreignId('departure_id')->nullable()->constrained()->nullOnDelete();
            $table->foreignId('user_id')->nullable()->constrained()->nullOnDelete();
            $table->foreignId('tour_id')->nullable()->constrained()->nullOnDelete();
            $table->foreignId('hotel_id')->nullable()->constrained()->nullOnDelete();
            $table->tinyInteger('method_id')->nullable()->constrained()->nullOnDelete();
            //========================================
            // Extras
            //========================================
            $table->string('payment_method');
            $table->decimal('first_payment',6,2)->nullable();
            $table->decimal('actual_pay',6,2)->nullable();
            $table->string('card',4)->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
