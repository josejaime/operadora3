<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('methods')->insert([
            'name' => 'Efectivo',
        ]);
        DB::table('methods')->insert([
            'name' => 'Tarjeta',
        ]);
        DB::table('methods')->insert([
            'name' => 'City Pass',
        ]);
        DB::table('methods')->insert([
            'name' => 'Cortesía',
        ]);
    }
}
