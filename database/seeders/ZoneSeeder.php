<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zones')->insert([
            'name' => 'Zona Sur',
            'number' => 1,
            'closure' => '15',
        ]);
        DB::table('zones')->insert([
            'name' => 'Zona Norte',
            'number' => 2,
            'closure' => '15',
        ]);
        DB::table('zones')->insert([
            'name' => 'Zona Este',
            'number' => 3,
            'closure' => '15',
        ]);
        DB::table('zones')->insert([
            'name' => 'Zona Oeste',
            'number' => 4,
            'closure' => '15',
        ]);
    }
}
