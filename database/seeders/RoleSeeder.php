<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'type' => 'Operador',
        ]);
        DB::table('roles')->insert([
            'type' => 'Módulo',
        ]);
        DB::table('roles')->insert([
            'type' => 'Recepción',
        ]);
    }
}
