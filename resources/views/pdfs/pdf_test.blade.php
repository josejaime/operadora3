<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Recolección</title>
    {{-- <link rel="stylesheet" href="{{ asset('lib/semantic/table.css') }}"> --}}
    <style media="screen">
        th, td {
            font-size: 11px;
            padding: 10px 0px;
        }
        tr.header td {
            border-bottom: 1px solid #ddd;
        }
        tr.footer td {
            border-top: 1px solid #ddd;
        }
        body, html {
            /*background-color: red;*/
            margin: 1%;
            padding: 1%;
        }
        .section, .container {
            margin-left: 0;
            margin-right: 0;
            padding-left: 0;
            padding-right: 0;
        }
        .section, table, thead, tbody {
            width: 100%;
            width: 100vw;
        }
        .section {
            /* background-color: yellow; */
        }
        .container {
            margin: 10px;
            /* background-color: green; */
            /* color: white; */
            width: 98%;
        }
        .letterhead h5 {
            padding: 10px;
            border-bottom: 1px solid #ddd;
            text-align: center;
            font-size: 18px;
        }
        .letterhead h6 {
            margin-top: 0;
            font-weight: 400;
            font-size: 16px;
        }
        .page-break {
            page-break-after: always;
        }
        * {
            /*border: 1px solid green;*/
        }
        h1,h2,h3,h4,h5,h6 {
            margin: 5px 0;
            padding: 5px 0;
        }
        h1.center ,h2.center ,h3.center ,h4.center ,h5.center ,h6.center  {
            text-align: center;
        }
        h1.caption ,h2.caption ,h3.caption ,h4.caption ,h5.caption ,h6.caption  {
            background-color: #f9fafb;;
            padding: 8px;
            border-radius: 2px;
            border: 1px solid rgba(34,36,38,.1);
        }
        @include('pdfs.styles')
        .no-border {
            border: none !important;
        }
    </style>
</head>
<body>
    <div class="section">
        <div class="container letterhead">
            <h5 width="100%" class="center">Operadora Zacatecas S.A de C.V</h5>
            <h6 width="100%" class="center"><b>Fecha:</b> <big>{{ $date }} | {{ \Carbon\Carbon::createFromTimeStamp(strtotime($date))->translatedFormat('d F Y') }}</big></h6>
            <h6 width="100%" class="caption"><b>Tour:</b> <big>{{ $tour->name }}</big> </h6>
        </div>
    </div>
    <div class="section">
        <div class="container">
            {{-- <h4>Reservaciones <small>[ conteo {{ $reservations->sum('number_kids') + $reservations->sum('number_adults') + $reservations->sum('number_elders') }} ]</small></h4> --}}
            <table class="ui table">
                <thead>
                    <tr>
                        <th scope="col">Folio</th>
                        <th scope="col">Cliente</th>
                        <th scope="col">Tour</th>
                        <th scope="col">Horario</th>
                        <th scope="col">Total</th>
                        <th scope="col">Resto</th>
                        <th scope="col">Hotel</th>
                        <th scope="col">Hab.</th>
                        <th scope="col">A</th>
                        <th scope="col">N</th>
                        <th scope="col">I</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($reservations as $key => $reservation)
                        <tr>
                            <td scope="row" data-label="Folio">{{ $reservation->folio }}</td>
                            <td data-label="Cliente">{{ $reservation->client }}</td>
                            <td data-label="Tour">{{ $reservation->tour->name }}</td>
                            <td data-label="Hora"><b>{{ $reservation->departure->hour }}</b></td>
                            <td data-label="Total">{{ $reservation->total }}</td>
                            <td data-label="Adeudo">{{ $reservation->remainig ? $reservation->remainig : 'N/A'  }}</td>
                            {{-- <td data-label="resto">{{ $reservation->method->name }}</td> --}}
                            <td data-label="Hotel">{{ $reservation->hotel->name }}</td>
                            <td data-label="Hab">{{ $reservation->room }}</td>
                            <td data-label="Adultos">{{ $reservation->adults }}</td>
                            <td data-label="Niños">{{ $reservation->kids }}</td>
                            <td data-label="INSEN">{{ $reservation->elders }}</td>
                            {{-- <td data-label="Teléfono">{{ $reservation->telephone }}</td> --}}
                            {{-- <td data-label="Usuario">{{ $reservation->username }}</td> --}}
                        </tr>

                        @if( $loop->last )
                            <tr>
                                <td colspan="8" class="no-border"></td>
                                <td> {{ $reservations->sum('adults') }} </td>
                                <td> {{ $reservations->sum('kids') }} </td>
                                <td> {{ $reservations->sum('elders') }} </td>
                            </tr>
                        @endif
                    @empty
                        <tr>
                            <td colspan="9" class="alert alert-danger">
                                No hay reservas para este tour
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
