<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        {{-- <link rel="stylesheet" href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}"> --}}
        <link rel="stylesheet" href="{{ asset('lib/semantic/semantic.min.css') }}">
        {{-- <link rel="stylesheet" href="{{ asset('lib/calendar/calendar.css') }}"> --}}
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mysass.css') }}">

        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        {{-- <script src="{{ asset('lib/bootstrap/js/bootstrap.min.js') }}" defer></script> --}}
        {{-- Font Awesome --}}
        <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/all.min.css') }}">
        <style>
        #snackbar {
          visibility: hidden;
          min-width: 250px;
          /* margin-left: -125px;
          background-color: #333;
          color: #fff;
          text-align: center;
          border-radius: 2px;
          padding: 16px; */
          position: fixed;
          z-index: 1;
          right: 10px;
          bottom: 150px;
          /* font-size: 17px;  */
        }

        #snackbar.show {
          visibility: visible;
          -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
          animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        @-webkit-keyframes fadein {
          from {bottom: 0; opacity: 0;}
          to {bottom: 150px; opacity: 1;}
        }

        @keyframes fadein {
          from {bottom: 0; opacity: 0;}
          to {bottom: 150px; opacity: 1;}
        }

        @-webkit-keyframes fadeout {
          from {bottom: 150px; opacity: 1;}
          to {bottom: 0; opacity: 0;}
        }

        @keyframes fadeout {
          from {bottom: 150px; opacity: 1;}
          to {bottom: 0; opacity: 0;}
        }
        </style>
        @stack('styles')
    </head>
    <body class="font-sans antialiased">
        <div class="ui left vertical menu sidebar">
            <a class="item">
                Item 1
            </a>
            <a class="item">
                Item 2
            </a>
            <a class="item">
                Item 3
            </a>
        </div>
        <div class="pusher">
            @include('layouts.navigation')

            <!-- Page Heading -->
            {{-- <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header> --}}

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
        <div class="spacer"></div>
        <footer>
            <div class="ui container">
                <p>
                    Desarrollado para Operadora 2021.
                </p>
            </div>
        </footer>
        <div id="snackbar" class="ui message yellow">Some text some message..</div>
    </body>
    <!-- Scripts -->
    <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('lib/semantic/semantic.min.js') }}" defer></script>
    <script src="{{ asset('lib/fontawesome/js/all.min.js') }}" defer></script>
    <script type="text/javascript">
        jQuery( document ).ready( function() {
            $('.ui.dropdown')
                .dropdown()
            ;

            $('.hamburger').on('click', function() {
                $('.ui.sidebar')
                  .sidebar('toggle')
                ;
            });

            $('form').on('submit', function() {
                $('form').addClass('loading');
            });
        });
        function showSnackBar(  message ) {
            var x = document.getElementById("snackbar");
            x.className = "show ui message yellow";
            x.innerText = message;
            setTimeout(function(){ x.className = x.className.replace("show ui message yellow", ""); }, 3000);
        }
    </script>
    @stack('scripts')
</html>
