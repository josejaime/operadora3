@if (session('green'))
    <div class="ui message green">
        <p>{{ session('green') }}</p>
    </div>
@endif
@if (session('yellow'))
    <div class="ui message yellow">
        <p>{{ session('yellow') }}</p>
    </div>
@endif
@if (session('red'))
    <div class="ui message red">
        <p>{{ session('red') }}</p>
    </div>
@endif
@if (session('purple'))
    <div class="ui message purple">
        <p>{{ session('purple') }}</p>
    </div>
@endif
@if (session('blue'))
    <div class="ui message blue">
        <p>{{ session('blue') }}</p>
    </div>
@endif
