<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Agregar Comisión
        </h4>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para agregar comisiones a <b>{{ $user->name }}</b>
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('layouts.messages')
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('commissions.store') }}">
            @csrf
            <div class="field">
                <label>Tour</label>
                <select class="ui fluid dropdown" name="tour_id">
                    @forelse ($tours as $key => $tour)
                        <option value="{{ $tour->id }}">{{ $tour->name }}</option>
                    @empty
                        <option class="ui message yellow">No hay tours por el momento</option>
                    @endforelse
                </select>
            </div>
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <div class="field">
                <label>Comisión niños</label>
                <input type="number" name="kids" placeholder="Comisión niños" min="0" value="0">
            </div>
            <div class="field">
                <label>Comisión adultos</label>
                <input type="number" name="adults" placeholder="Comisión adultos" min="0" value="0">
            </div>
            <div class="field">
                <label>Comisión INSEN</label>
                <input type="number" name="elders" placeholder="Comisión INSEN" min="0" value="0">
            </div>
            <button class="ui button purple" type="submit">Guardar</button>
        </form>

    </div>

</x-app-layout>
