<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Editar Comisión
        </h4>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para editar la comisión a <b>{{ $commission->user->name }}</b>
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('commissions.update', ['commission' => $commission]) }}">
            @csrf
            @method('PUT')
            <div class="field">
                <label>Tour</label>
                <input type="text" placeholder="Nombre del Tour" value="{{ $commission->tour->name }}" disabled>
            </div>
            {{-- <input type="hidden" name="tour_id" value="{{ $commission->tour->id }}">
            <input type="hidden" name="user_id" value="{{ $commission->user->id }}"> --}}
            <div class="field">
                <label>Comisión niños</label>
                <input type="number" name="kids" placeholder="Comisión niños" min="0" value="{{ $commission->kids }}">
            </div>
            <div class="field">
                <label>Comisión adultos</label>
                <input type="number" name="adults" placeholder="Comisión adultos" min="0" value="{{ $commission->adults }}">
            </div>
            <div class="field">
                <label>Comisión INSEN</label>
                <input type="number" name="elders" placeholder="Comisión INSEN" min="0" value="{{ $commission->elders }}">
            </div>
            <button class="ui button purple" type="submit">Guardar</button>
        </form>

    </div>

</x-app-layout>
