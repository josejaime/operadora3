<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Agregar Hotel
        </h4>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para agregar un hotel
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('tours.store') }}">
            @csrf
            <div class="field">
                <label>Nombre</label>
                <input type="text" name="name" placeholder="Nombre del tour" value="{{ old('name') }}">
            </div>
            <div class="field">
                <label>Precio niños</label>
                <input type="number" name="cost_kids" placeholder="Precio niños" value="{{ old('cost_kids') }}">
            </div>
            <div class="field">
                <label>Precio adultos</label>
                <input type="number" name="cost_adults" placeholder="Precio adultos" value="{{ old('cost_adults') }}">
            </div>
            <div class="field">
                <label>Precio INSEN</label>
                <input type="number" name="cost_elders" placeholder="Precio INSEN" value="{{ old('cost_elders') }}">
            </div>
            <div class="field">
                <label>Límite (* No lo llenes si no tiene límite)</label>
                <input type="number" name="limit" placeholder="Límite de lugares, si es apicable" value="{{ old('limit') }}">
            </div>
            <div class="field">
                <label>Compañía</label>
                    <select class="ui fluid dropdown" name="company_id">
                        @forelse ($companies as $key => $company)
                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @empty
                            <option class="ui message yellow" value="null">No hay compañías por el momento</option>
                        @endforelse
                    </select>
            </div>
            <button class="ui button purple" type="submit">Guardar</button>
        </form>

    </div>

</x-app-layout>
