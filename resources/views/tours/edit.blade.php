<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Editar Tour
        </h4>
    </div>

    <div class="ui container page-description">
        {{-- <h4>
            Todos los usuarios en el sistema
        </h4> --}}
        <p>
            Usa esta seccion para editar el tour
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('layouts.messages')
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('tours.update', ['tour' => $tour]) }}">
            @csrf
            @method('PUT')
            <div class="field">
                <label>Nombre</label>
                <input type="text" name="name" placeholder="Nombre" value="{{ $tour->name }}">
            </div>
            <div class="field">
                <label>Precio Niños</label>
                <input type="number" name="cost_kids" placeholder="Precio para niños" value="{{ $tour->cost_kids }}">
            </div>
            <div class="field">
                <label>Precio Adultos</label>
                <input type="number" name="cost_adults" placeholder="Precio para adultos" value="{{ $tour->cost_adults }}">
            </div>
            <div class="field">
                <label>Precio INSEN</label>
                <input type="number" name="cost_elders" placeholder="Precio para niños" value="{{ $tour->cost_elders }}">
            </div>
            <div class="field">
                <label>Límite</label>
                <input type="number" name="limit" placeholder="Límite" value="{{ $tour->limit }}">
            </div>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" name="active" {{ $tour->active ? 'checked' : ''}}>
                    <label>Activo</label>
                </div>
            </div>
            @if ($tour->company->id == 2)
                <div class="field">
                    <div class="ui checkbox">
                        <input type="checkbox" name="is_fast" {{ $tour->is_fast ? 'checked' : ''}}>
                        <label>¿Fast Track?</label>
                    </div>
                </div>
            @endif
            <button class="ui button yellow" type="submit">Actualizar</button>
        </form>

    </div>

    <div class="ui main container">
        <h3>Horarios</h3>
        <div class="ui text-right">
            <a href="{{ route('departures.create') }}?tour={{$tour->id}}">
                <i class="fas fa-2x fa-plus-square"></i>&nbsp; Agregar horario
            </a>
        </div>
        <div class="spacer">
            <p>Da click en alguno para activarlo o desactivarlo</p>
            <ul>
                <li>Verde - activo</li>
                <li>Rojo - desactivado</li>
            </ul>
        </div>
        <div class="grid-departures">
            @forelse ($tour->departures->sortBy('hour') as $key => $departure)
                <div class="ui hour grey">
                    <a class="ui button {{ $departure->closed ? 'red' : 'green'}}" href="{{ route('departures.close', ['departure' => $departure]) }}" style="width: 80%;">
                        {{ $departure->hour }}
                    </a>
                    @if ( $tour->company->id == 2 )
                        <a href="#!" class="button ui purple action" id="modal{{ $departure->id }}" onclick="openModal({{ $departure->id }})" style="width:35%;">
                            <i class="fas fa-bus" style="color: white;"></i>
                                {{ $departure->type == 1 ? '51' : '61' }}
                        </a>
                    @endif
                    <a href="#!" class="button ui black action" id="modal{{ $departure->id }}" onclick="openModal({{ $departure->id }})">
                        <i class="fas fa-trash" style="color: white;"></i>
                    </a>
                </div>
            @empty
                <div class="ui message yellow">
                    No hay horarios para este tour
                </div>
            @endforelse
        </div>

    </div>
    @foreach ($tour->departures as $key => $departure)
        <div class="ui mini modal" id="{{ $departure->id }}">
            <div class="header">¿Borrar Horario?</div>
            <div class="content">
                <p>
                    {{ $departure->hour }}
                </p>
            </div>
            <div class="actions">
                <form action="{{ route('departures.destroy', ['departure'=>$departure]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">

            $(document).ready( function() {
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
