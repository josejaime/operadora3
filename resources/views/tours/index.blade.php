<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Admin Tours
        </h4>
    </div>

    <div class="ui container page-description">
        <h4>
            Todos los Tours en el sistema
        </h4>
        <p>
            Usa esta seccion para agregar o modificar tours
        </p>
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <div class="ui text-right">
            <a href="{{ route('tours.create') }}">
                <i class="fas fa-2x fa-plus-square"></i>
            </a>
        </div>
        <table class="ui yellow table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Precio Niños</th>
                    <th>Precio Adultos</th>
                    <th>Precio INSEN</th>
                    <th>Límite</th>
                    <th class="right aligned">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($tours as $key => $tour)
                    <tr class="{{ $tour->active ? '' : 'disabled'}}">
                        <td>
                            {{ $tour->name }}<br>
                            <small>
                                <b>({{ $tour->company->name }})</b>
                            </small>
                        </td>
                        <td>{{ $tour->cost_kids }}</td>
                        <td>{{ $tour->cost_adults }}</td>
                        <td>{{ $tour->cost_elders }}</td>
                        <td>{{ $tour->limit ? $tour->limit : 'N/A' }}</td>
                        <td class="right aligned" style="pointer-events: all;">
                            <a href="{{ route('tours.edit', ['tour' => $tour->id]) }}">
                                <i class="fas fa-2x fa-eye"></i>
                            </a>
                            {{-- @if ($zone->hotels_count == 0) --}}
                                <a href="#!" id="modal{{ $tour->id }}" onclick="openModal({{ $tour->id }})">
                                    <i class="fas fa-2x fa-trash-alt"></i>
                                </a>
                            {{-- @endif --}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10" class="ui message warning">
                            No hay tours en el sistema
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="ui container text text-center">
        {!! $tours->links('layouts.pagination') !!}
        {{-- {!! $zones->links() !!} --}}
    </div>

    @foreach ($tours as $key => $tour)
        <div class="ui mini modal" id="{{ $tour->id }}">
            <div class="header">¿Borrar Tour?</div>
            <div class="content">
                <p>
                    {{ $tour->name }}
                </p>
            </div>
            <div class="actions">
                <form action="{{ route('tours.destroy', ['tour'=>$tour]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">

            $(document).ready( function() {
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
