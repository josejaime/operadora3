<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Agregar Hotel
        </h4>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para agregar un hotel
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('hotels.store') }}">
            @csrf
            <div class="field">
                <label>Nombre</label>
                <input type="text" name="name" placeholder="Nombre del hotel/lugar" value="{{ old('name') }}">
            </div>
            <div class="field">
                <label>Abreviación</label>
                <input type="text" name="key" placeholder="Abreviación a dos letras" value="{{ old('key') }}">
            </div>
            <div class="field">
                <label>Zona</label>
                <select class="ui fluid dropdown" name="zone_id">
                    @forelse ($zones as $key => $zone)
                        <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                    @empty
                        <option class="ui message yellow">No hay zonas por el momento</option>
                    @endforelse
                </select>
            </div>
            <button class="ui button purple" type="submit">Guardar</button>
        </form>

    </div>

</x-app-layout>
