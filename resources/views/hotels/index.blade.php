<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Admin Hoteles
        </h4>
    </div>

    <div class="ui container page-description">
        <h4>
            Todos los Hoteles en el sistema
        </h4>
        <p>
            Usa esta seccion para agregar o modificar hoteles
        </p>
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <div class="ui text-right">
            <a href="{{ route('hotels.create') }}">
                <i class="fas fa-2x fa-plus-square"></i>
            </a>
        </div>
        <table class="ui yellow table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Zona</th>
                    <th class="right aligned">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($hotels as $key => $hotel)
                    <tr>
                        <td>{{ $hotel->name }} - ({{ $hotel->key }})</td>
                        <td>{{ $hotel->zone ? $hotel->zone->name : 'Sin asignar' }}</td>
                        <td class="right aligned">
                            <a href="{{ route('hotels.edit', ['hotel' => $hotel->id]) }}">
                                <i class="fas fa-2x fa-eye"></i>
                            </a>
                            <a href="#!" id="modal{{ $hotel->id }}" onclick="openModal({{ $hotel->id }})">
                                <i class="fas fa-2x fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10" class="ui message warning">
                            No hay hoteles en el sistema
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="ui container text text-center">
        {!! $hotels->links('layouts.pagination') !!}
    </div>

    @foreach ($hotels as $key => $hotel)
        <div class="ui mini modal" id="{{ $hotel->id }}">
            <div class="header">¿Borrar Hotel?</div>
            <div class="content">
                <p>
                    {{ $hotel->name }}
                </p>
            </div>
            <div class="actions">
                <form action="{{ route('hotels.destroy', ['hotel'=>$hotel->id]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">

            $(document).ready( function() {
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
