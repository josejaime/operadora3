<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Modificar Hotel
        </h4>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para modificar un hotel
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('hotels.update', ['hotel' => $hotel]) }}">
            @csrf
            @method('PUT')
            <div class="field">
                <label>Nombre</label>
                <input type="text" name="name" placeholder="Nombre del hotel/lugar" value="{{ $hotel->name }}">
            </div>
            <div class="field">
                <label>Abreviación</label>
                <input type="text" placeholder="Abreviación a dos letras" value="{{ $hotel->key }}" disabled>
            </div>
            <div class="field">
                <label>Zona</label>
                <select class="ui fluid dropdown" name="zone_id">
                    @forelse ($zones as $key => $zone)
                        <option value="{{ $zone->id }}" {{ $hotel->zone->id == $zone->id ? 'selected' : '' }}>{{ $zone->name }}</option>
                    @empty
                        <option class="ui message yellow">No hay zonas por el momento</option>
                    @endforelse
                </select>
            </div>
            <button class="ui button yellow" type="submit">Actualizar</button>
        </form>

    </div>

    <div class="container ui main">
        <h3>Usuarios de este Hotel</h3>
        <ul>
            @forelse ($hotel->users as $key => $user)
                <li>{{ $user->name }} <b>({{ $user->username}})</b></li>
            @empty
                <div class="ui message blue">
                    <p>
                        No hay usuarios registrados para este hotel
                    </p>
                </div>
            @endforelse
        </ul>
    </div>

</x-app-layout>
