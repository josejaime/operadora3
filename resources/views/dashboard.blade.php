<x-app-layout>
    {{-- <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ Auth::user()->username }} {{ __('¡estás logeado!') }}
        </h2>
    </x-slot> --}}

    <div class="subtitle-header">
        <h4>
            ¡Hola, {{ Auth::user()->username }}!
        </h4>
    </div>

    <div class="ui container page-description">
        <h4>
            ¿Qué te gustaría hacer?
        </h4>
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <h2>Reservaciones</h2>
        <div class="grid-container">
            <a href="{{ route('reservations.select.tour') }}" class="action ui button purple">
                Crear nueva
            </a>
            <a href="{{ route('reservations.select.tour.review') }}" class="action ui button purple">
                Revisar tour
            </a>
            <a href="#!" class="action ui button purple">
                Confirma
            </a>
            <a href="#!" class="action ui button purple">
                Mis reservaciones
            </a>
        </div>
        <h2>Reportes</h2>
        <div class="grid-container">
            <a href="#!" class="action ui button violet">
                Recolección
            </a>
        </div>
        <h2>Administración rápida</h2>
        <div class="grid-container">
            <a href="{{ route('users.index') }}" class="action ui button pink">
                Usuarios
            </a>
            <a href="{{ route('zones.index') }}" class="action ui button pink">
                Zonas
            </a>
            <a href="{{ route('hotels.index') }}" class="action ui button pink">
                Hoteles
            </a>
            <a href="{{ route('tours.index') }}" class="action ui button pink">
                Tours
            </a>
        </div>
        <h2>Acciones</h2>
        <div class="grid-container">
            <a href="#!" class="action ui button orange">
                Logs
            </a>
        </div>
        <h2>Reportes</h2>
        <div class="grid-container">
            <a href="#!" class="ui action button teal">
                Reportes Ventas de Tours
            </a>
            <a href="#!" class="ui action button teal">
                Reportes Ventas de Usuario
            </a>
            <a href="#!" class="ui action button teal">
                Reportes Ventas de Hoteles
            </a>
            <a href="#!" class="ui action button teal">
                Reportes Ventas
            </a>
            <a href="#!" class="ui action button teal">
                Confirmadas contra no confirmadas
            </a>
            <a href="#!" class="ui action button teal">
                Reporte de Ventas del día Operadora
            </a>
            <a href="#!" class="ui action button teal">
                Reporte de Ventas del día Maxibus
            </a>
        </div>
    </div>
</x-app-layout>
