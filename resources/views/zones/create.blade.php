<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Agregar Zona
        </h4>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para agregar una zona
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('zones.store') }}">
            @csrf
            <div class="field">
                <label>Número</label>
                <input type="number" name="number" placeholder="Número" value="{{ old('number') }}">
            </div>
            <div class="field">
                <label>Nombre de la zona</label>
                <input type="text" name="name" placeholder="Nombre" value="{{ old('name') }}">
            </div>
            <div class="field">
                <label>Tiempo de cierre antes de la salida de los tours</label>
                <input type="number" name="closure" placeholder="Cierre..." value="{{ old('closure') }}">
            </div>
            <button class="ui button purple" type="submit">Guardar</button>
        </form>

    </div>

</x-app-layout>
