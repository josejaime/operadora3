<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Editar Zona
        </h4>
    </div>

    <div class="ui container page-description">
        {{-- <h4>
            Todos los usuarios en el sistema
        </h4> --}}
        <p>
            Usa esta seccion para editar la zona
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('zones.update', ['zone' => $zone->id]) }}">
            @csrf
            @method('PUT')
            <div class="field">
                <label>Nombre de la zona</label>
                <input type="text" name="name" placeholder="Nombre" value="{{ $zone->name }}">
            </div>
            <div class="field">
                <label>Tiempo de cierre antes de la salida de los tours</label>
                <input type="number" name="closure" placeholder="Cierre..." value="{{ $zone->closure }}">
            </div>
            <button class="ui button yellow" type="submit">Actualizar</button>
        </form>

    </div>

    <div class="ui main container">
        <h3>Hoteles</h3>
        <ul>
            @forelse ($zone->hotels as $key => $hotel)
                <li>
                    {{ $hotel->name }}
                </li>
            @empty
                <div class="ui message yellow">
                    No hay hoteles en esta zona
                </div>
            @endforelse
        </ul>


    </div>

    @push('scripts')
        {{-- <script type="text/javascript">
            $( document ).ready( function(){
                $('form').on('submit', function() {
                    $('form').addClass('loading');
                });
            });
        </script> --}}
    @endpush

</x-app-layout>
