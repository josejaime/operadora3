<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Admin Zones
        </h4>
    </div>

    <div class="ui container page-description">
        <h4>
            Todas las zonas en el sistema
        </h4>
        <p>
            Usa esta seccion para agregar o modificar zonas
        </p>
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <div class="ui text-right">
            <a href="{{ route('zones.create') }}">
                <i class="fas fa-2x fa-plus-square"></i>
            </a>
        </div>
        <table class="ui yellow table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cierre <small>*si aplicable</small></th>
                    <th class="right aligned">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($zones as $key => $zone)
                    <tr>
                        <td>{{ $zone->number }} - {{ $zone->name }}</td>
                        <td>{{ $zone->closure }}min</td>
                        <td class="right aligned">
                            <a href="{{ route('zones.edit', ['zone' => $zone->id]) }}">
                                <i class="fas fa-2x fa-eye"></i>
                            </a>
                            {{-- @if ($zone->hotels_count == 0) --}}
                                <a href="#!" id="modal{{ $zone->id }}" onclick="openModal({{ $zone->id }})">
                                    <i class="fas fa-2x fa-trash-alt"></i>
                                </a>
                            {{-- @endif --}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10" class="ui message warning">
                            No hay zonas en el sistema
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="ui container text text-center">
        {!! $zones->links('layouts.pagination') !!}
        {{-- {!! $zones->links() !!} --}}
    </div>

    @foreach ($zones as $key => $zone)
        <div class="ui mini modal" id="{{ $zone->id }}">
            <div class="header">¿Borrar Zona?</div>
            <div class="content">
                <p>
                    {{ $zone->name }}

                </p>
            </div>
            <div class="actions">
                <form action="{{ route('zones.destroy', ['zone'=>$zone->id]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">

            $(document).ready( function() {
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
