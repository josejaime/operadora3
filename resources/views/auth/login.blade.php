<x-guest-layout>
    <style media="screen">
        .login-form {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100vw;
            height: calc( 100vh - 113px );
        }
        .card {
            min-width: 350px;
            padding: 15px !important;
        }
        .header {
            margin: 15px 0px;
        }
    </style>
    <div class="ui blue large inverted menu" style="margin:0;">
        <x-nav-link>
            {{ __('Bienvenido a Operadora') }}
        </x-nav-link>
    </div>
    </div>
    <div class="login-form">
        <div class="ui centered card">
            <div class="content">
                <div class="header">Inicia Sesión</div>
                <!-- Session Status -->
                <x-auth-session-status class="mb-4" :status="session('status')" />

                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <form method="POST" action="{{ route('login') }}" class="ui form">
                    @csrf

                    {{-- <!-- Email Address -->
                    <div>
                    <x-label for="email" :value="__('Email')" />

                    <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                </div> --}}

                <!-- Username -->
                <div>
                    <x-label for="username" :value="__('Nombre de Usuario')" />

                    <x-input id="username" class="block mt-1 w-full" type="text" name="username" :value="old('username')" required autofocus />
                </div>

                <!-- Password -->
                <div class="mt-4">
                    <x-label for="password" :value="__('Contraseña')" />

                    <x-input id="password" class="block mt-1 w-full"
                    type="password"
                    name="password"
                    required autocomplete="current-password" />
                </div>

                <!-- Remember Me -->
                <br>
                <div class="field">
                    <label for="remember_me" class="inline-flex items-center">
                        <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                        <span class="ml-2 text-sm text-gray-600">{{ __('Recordarme') }}</span>
                    </label>
                </div>

                <div class="field">
                    @if (Route::has('password.request'))
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                            {{ __('¿olvidé mi contraseña?') }}
                        </a>
                    @endif
                    <br>
                    <br>
                    <x-button class="ui button green">
                        {{ __('Log in') }}
                    </x-button>
                </div>
            </form>
        </div>
    </div>
</div>
<footer>
    <div class="ui container">
        <p>
            Desarrollado para Operadora 2021.
        </p>
    </div>
</footer>
</x-guest-layout>
