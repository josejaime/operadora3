<x-app-layout>
    <div class="subtitle-header">
        {{-- <h4>
            Reservaciones para una fecha especifica
        </h4> --}}
    </div>

    <div class="ui container page-description">
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <h3>
            Reservas de: {{ Auth::user()->name }}
        </h3>
    </div>

    <div class="ui container main">
        <table class="ui blue table">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Nombre</th>
                    <th>Tour</th>
                    <th>Hotel</th>
                    <th>A</th>
                    <th>N</th>
                    <th>I</th>
                    <th>Total</th>
                    <th class="right aligned">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($reservations as $key => $reservation)
                    <tr>
                        <td>{{ $reservation->folio }}</td>
                        <td>{{ $reservation->client }}</td>
                        <td>{{ $reservation->tour->name }}</td>
                        <td>{{ $reservation->hotel->name }}</td>
                        <td>{{ $reservation->adults }}</td>
                        <td>{{ $reservation->kids }}</td>
                        <td>{{ $reservation->elders }}</td>
                        <td>{{ $reservation->total }}</td>
                        <td class="right aligned" style="pointer-events: all;">
                            {{-- <a href="{{ route('users.show', ['reservation' => $reservation]) }}"> --}}
                            <a href="{{ route('reservations.show', ['reservation' => $reservation]) }}">
                                <i class="fas fa-2x fa-eye"></i>
                            </a>
                            <a href="#!" id="modal{{ $reservation->id }}" onclick="openModal({{ $reservation->id }})">
                                <i class="fas fa-2x fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10" class="ui message yellow">
                            No hay reservaciones para este tour
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="ui container text text-center">
        {!! $reservations->links('layouts.pagination') !!}
    </div>

    @foreach ($reservations as $key => $reservation)
        <div class="ui mini modal" id="{{ $reservation->id }}">
            <div class="header">¿Borrar Reservación?</div>
            <div class="content">
                <p>
                    {{ $reservation->folio }}
                </p>
                <p>
                    <b>Cliente</b><br>
                    {{ $reservation->client }}
                </p>
                <p>
                    <small>
                        <i>*Una vez borrada no se puede recuperar</i>
                    </small>
                </p>
            </div>
            <div class="actions">
                <form action="{{ route('reservations.destroy', ['reservation' => $reservation]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">
            $(document).ready(function(){
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
