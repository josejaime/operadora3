<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Ver Usuario
        </h4>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para editar el usuario
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @include('layouts.messages')
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('users.update', ['user' => $user]) }}">
            @csrf
            @method('PUT')
            <div class="field">
                <label>Nombre</label>
                <input type="text" name="name" placeholder="Nombre" required value="{{ $user->name }}">
            </div>
            <div class="field">
                <label>Username</label>
                <input type="text" name="name" disabled value="{{ $user->username }}">
            </div>
            <div class="field">
                <label>Rol</label>
                <select class="ui fluid dropdown" name="role_id">
                    @forelse ($roles as $key => $role)
                        <option value="{{ $role->id }}" {{ $role->id == $user->role->id ? 'selected' : '' }}>{{ $role->type }}</option>
                    @empty
                        <option class="ui message yellow">No hay roles por el momento</option>
                    @endforelse
                </select>
            </div>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" name="active" {{ $user->active ? 'checked' : ''}}>
                    <label>Activo</label>
                </div>
            </div>
            <div class="field">
                <label>Hotel</label>
                    <select class="ui fluid dropdown" name="hotel_id">
                        @if ( empty($user->hotel) )
                            <option value="-1" selected>Sin Asignar</option>
                        @endif
                        @forelse ($hotels as $key => $hotel)
                            @if ( empty($user->hotel) )
                                <option value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                            @else
                                <option value="{{ $hotel->id }}" {{ $hotel->id == $user->hotel->id ? 'selected' : '' }}>{{ $hotel->name }}</option>
                            @endif
                        @empty
                            <option class="ui message yellow">No hay hoteles por el momento</option>
                        @endforelse
                    </select>
            </div>
            <div class="field">
                <label>Compañía</label>
                    <select class="ui fluid dropdown" name="company_id">
                        @forelse ($companies as $key => $company)
                            @if ($user->company)
                                <option value="{{ $company->id }}" {{ $company->id == $user->company->id ? 'selected' : '' }}>{{ $company->name }}</option>
                            @else
                                <option value="">No asigando</option>
                            @endif
                        @empty
                            <option class="ui message yellow">No hay compañías por el momento</option>
                        @endforelse
                    </select>
            </div>
            <button class="ui button yellow" type="submit">Actualizar</button>
        </form>

    </div>

    <div class="container ui main">
        <h3>Más información</h3>
        <div class="grid-container">
            <a href="#!" class="action ui button red">Reservaciones</a>
            <a href="#!" class="action ui button green">Pagos Recibidos</a>
        </div>
    </div>

    <div class="ui container main">
        <h3>Comisiones</h3>
        {{-- <div class="grid">
            <a href="#!" class="ui button purple action">Agregar comisión</a>
        </div> --}}
        <div class="ui text-right">
            <a href="{{ route('commissions.create', ['user' => $user]) }}">
                <i class="fas fa-2x fa-plus-square"></i>&nbsp; Agregar comisión
            </a>
        </div>
        <table class="ui yellow table">
            <thead>
                <tr>
                    <th>Tour</th>
                    <th>Comisión Niños</th>
                    <th>Comisión Adultos</th>
                    <th>Comisión INSEN</th>
                    <th class="right aligned">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($user->commissions as $key => $commission)
                    <tr>
                        <td>{{ $commission->tour->name }}</td>
                        <td>{{ $commission->kids }}</td>
                        <td>{{ $commission->adults }}</td>
                        <td>{{ $commission->elders }}</td>
                        <td class="right aligned" style="pointer-events: all;">
                            <a href="{{ route('commissions.edit', ['commission' => $commission]) }}">
                                <i class="fas fa-2x fa-eye"></i>
                            </a>
                            <a href="#!" id="modal{{ $commission->id }}" onclick="openModal({{ $commission->id }})">
                                <i class="fas fa-2x fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10" class="ui message warning">
                            No hay comisiones en el sistema
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>

    @foreach ($user->commissions as $key => $commission)
        <div class="ui mini modal" id="{{ $commission->id }}">
            <div class="header">¿Borrar comisión?</div>
            <div class="content">
                <p>
                    Comision para el tour: <br>
                    {{ $commission->tour->name }}
                </p>
            </div>
            <div class="actions">
                <form action="{{ route('commissions.destroy', ['commission'=> $commission]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">

            $(document).ready( function() {
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
