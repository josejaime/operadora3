<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Ver Usuario
        </h4>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para editar el usuario
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('users.store') }}" autocomplete="off">
            @csrf
            <div class="field">
                <label>Nombre</label>
                <input type="text" name="name" placeholder="Nombre" required value="{{ old('name') }}">
            </div>
            <div class="field">
                <label>Username</label>
                <input type="text" name="username" placeholder="Username" value="{{ old('username') }}" autocomplete="off">
            </div>
            <div class="field">
                <label>Email</label>
                <input type="email" name="email" placeholder="Email" value="{{ old('email') }}">
            </div>
            <div class="field">
                <label>Rol</label>
                <select class="ui fluid dropdown" name="role_id">
                    @forelse ($roles as $key => $role)
                        <option value="{{ $role->id }}">{{ $role->type }}</option>
                    @empty
                        <option class="ui message yellow">No hay roles por el momento</option>
                    @endforelse
                </select>
            </div>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" name="active" checked>
                    <label>Activo</label>
                </div>
            </div>
            <div class="field">
                <label>Hotel</label>
                    <select class="ui fluid dropdown" name="hotel_id">
                        <option value="null">Sin Asignar</option>
                        @forelse ($hotels as $key => $hotel)
                            <option value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                        @empty
                            <option class="ui message yellow" value="null">No hay roles por el momento</option>
                        @endforelse
                    </select>
            </div>
            <div class="field">
                <label>Compañía</label>
                    <select class="ui fluid dropdown" name="company_id">
                        @forelse ($companies as $key => $company)
                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @empty
                            <option class="ui message yellow" value="null">No hay compañías por el momento</option>
                        @endforelse
                    </select>
            </div>
            <div class="field">
                <label>Password</label>
                <input type="password" name="password" placeholder="Password" autocomplete="off">
            </div>
            <div class="field">
                <label>Confirma el Password</label>
                <input type="password" name="password_confirmation" placeholder="Confirma password">
            </div>
            <button class="ui button green" type="submit">Crear</button>
        </form>

    </div>

</x-app-layout>
