<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Bienvenido a tu perfil
        </h4>
    </div>

    <div class="ui container page-description">
        {{-- <h4>
            Todos los roles en el sistema
        </h4> --}}
        <p>
            Desde aquí puedes ver tu información
        </p>
        @if (session('status'))
            <div class="alert alert-success">
                <div class="ui message {{ session('class') ? session('class') : '' }}">
                    <p>{{ session('status') }}</p>
                </div>
            </div>
        @endif
    </div>

    <div class="ui container main">
        <form class="ui form" method="post" action="{{ route('users.update.profile', ['user' => Auth::user()->id]) }}">
            @csrf
            @method('PUT')
            <div class="disabled field">
                <label>Usuario</label>
                <input type="text" name="username" placeholder="Nombre de usuario" readonly value="{{ $user->username }}">
            </div>
            <div class="field">
                <label>Nombre</label>
                <input type="text" name="name" placeholder="Nombre" required value="{{ $user->name }}">
            </div>
            <div class="field">
                <label>Email</label>
                <input type="text" required value="{{ $user->email ? $user->email : 'N/A' }}">
            </div>
            <div class="field">
                <label>Rol</label>
                <input type="text" required value="{{ $user->role->type }}" readonly disabled>
            </div>
            <div class="field">
                <label>Companía</label>
                <input type="text" required value="{{ $user->company ? $user->company->name : 'No Asignada' }}" readonly disabled>
            </div>
            <div class="field">
                <label>Hotel</label>
                <input type="text" required value="{{ $user->hotel ? $user->hotel->name : 'No Asignado' }}" readonly disabled>
            </div>
            <button class="ui button yellow" type="submit">Actualizar</button>
        </form>

    </div>

    <div class="ui container main">
        <h3>Acciones</h3>
        <div class="grid-container">
            <a href="{{ route('users.reservations') }}" class="action ui button purple">
                Mis reservas
            </a>
        </div>
    </div>

    <div class="ui container main">
        <h3>¿Salir?</h3>
        <div class="grid-container">
            {{-- <a href="#!" class="action ui button red" onclick="event.preventDefault();
            this.closest('form').submit();"> --}}
            <form method="POST" action="{{ route('logout') }}" class="logOutForm" style="display:none;">
                @csrf
            </form>
            <a href="#" id="makeLogOut" class="action ui button red">
                Cerrar sesión <i class="fas fa-sign-out-alt"></i>
            </a>
            {{-- </a> --}}
        </div>
    </div>
    @push('scripts')
        <script type="text/javascript">
            $('#makeLogOut').on('click', function() {
                $('form.logOutForm').submit();
            });
        </script>
    @endpush

</x-app-layout>
