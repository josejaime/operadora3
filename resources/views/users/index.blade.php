<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Admin Usuarios
        </h4>
    </div>

    <div class="ui container page-description">
        <h4>
            Todos los usuarios en el sistema
        </h4>
        <p>
            Usa esta seccion para agregar o modificar usuarios
        </p>
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <div class="ui text-right">
            <a href="{{ route('users.create') }}">
                <i class="fas fa-2x fa-plus-square"></i>
            </a>
        </div>
        <table class="ui blue table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    {{-- <th>Compañía</th> --}}
                    <th>Rol</th>
                    <th>Hotel</th>
                    <th class="right aligned">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($users as $key => $user)
                    <tr class="{{ $user->active ? '' : 'disabled' }}">
                        <td>{{ $user->name }}
                            <br>
                            @if ($user->company)
                                <small><b>({{ $user->company->name }})</b></small>
                            @endif
                        </td>
                        <td>{{ $user->username }}</td>
                        {{-- <td>{{ $user->company_id ? $user->company_id : 'No asignada' }}</td> --}}
                        <td>{{ $user->role->type }}</td>
                        <td>{{ $user->hotel ? $user->hotel->name : 'No Asigando' }}</td>
                        <td class="right aligned" style="pointer-events: all;">
                            <a href="{{ route('users.show', ['user' => $user->id]) }}">
                                <i class="fas fa-2x fa-eye"></i>
                            </a>
                            <a href="#!" id="modal{{ $user->id }}" onclick="openModal({{ $user->id }})">
                                <i class="fas fa-2x fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10" class="ui message yellow">
                            No hay usuarios en el sistema :(
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="ui container text text-center">
        {!! $users->links('layouts.pagination') !!}
    </div>

    @foreach ($users as $key => $user)
        <div class="ui mini modal" id="{{ $user->id }}">
            <div class="header">¿Borrar Usuario?</div>
            <div class="content">
                <p>
                    {{ $user->name }}
                </p>
            </div>
            <div class="actions">
                <form action="{{ route('users.destroy', ['user'=>$user]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">

            $(document).ready( function() {
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
