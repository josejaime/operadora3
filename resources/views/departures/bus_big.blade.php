<style>
    .fas {
        padding: 0;
    }
</style>
<div class="ui grid bus-layout">
    <div class="seats first-floor">
        <div class="ui grid">
            <h3>Parte de arriba</h3>
        </div>
        <div class="ui grid">
            <span>
                <i id="38" class="fas fa-2x fa-couch available"></i>
                <div>38</div>
            </span>
            <span>
                <i id="37" class="fas fa-2x fa-couch available"></i>
                <div>37</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="none" class="fas fa-2x fa-shoe-prints taken" style="font-size: 2rem;"></i>
                <div>Entrada</div>
            </span>
            <span>
                <i id="none" class="fas fa-2x fa-couch" style="color: transparent;"></i>
            </span>
        </div>
        <div class="ui grid">
            <span class="">
                <i id="36" class="fas fa-2x fa-couch available"></i>
                <div>36</div>
            </span>
            <span class="">
                <i id="35" class="fas fa-2x fa-couch available"></i>
                <div>35</div>
            </span>
            <span class="hall"></span>
            <span class="">
                <i id="34" class="fas fa-2x fa-couch available"></i>
                <div>34</div>
            </span>
            <span class="">
                <i id="33" class="fas fa-2x fa-couch available"></i>
                <div>33</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="32" class="fas fa-2x fa-couch available"></i>
                <div>32</div>
            </span>
            <span>
                <i id="31" class="fas fa-2x fa-couch available"></i>
                <div>31</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="30" class="fas fa-2x fa-couch available"></i>
                <div>30</div>
            </span>
            <span>
                <i id="29" class="fas fa-2x fa-couch available"></i>
                <div>29</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="28" class="fas fa-2x fa-couch available"></i>
                <div>28</div>
            </span>
            <span>
                <i id="27" class="fas fa-2x fa-couch available"></i>
                <div>27</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="26" class="fas fa-2x fa-couch available"></i>
                <div>26</div>
            </span>
            <span>
                <i id="25" class="fas fa-2x fa-couch available"></i>
                <div>25</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="24" class="fas fa-2x fa-couch available"></i>
                <div>24</div>
            </span>
            <span>
                <i id="23" class="fas fa-2x fa-couch available"></i>
                <div>23</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="22" class="fas fa-2x fa-couch available"></i>
                <div>22</div>
            </span>
            <span>
                <i id="21" class="fas fa-2x fa-couch available"></i>
                <div>21</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="20" class="fas fa-2x fa-couch available"></i>
                <div>20</div>
            </span>
            <span>
                <i id="19" class="fas fa-2x fa-couch available"></i>
                <div>19</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="18" class="fas fa-2x fa-couch available"></i>
                <div>18</div>
            </span>
            <span>
                <i id="17" class="fas fa-2x fa-couch available"></i>
                <div>17</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="16" class="fas fa-2x fa-couch available"></i>
                <div>16</div>
            </span>
            <span>
                <i id="15" class="fas fa-2x fa-couch available"></i>
                <div>15</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="14" class="fas fa-2x fa-couch available"></i>
                <div>14</div>
            </span>
            <span>
                <i id="13" class="fas fa-2x fa-couch available"></i>
                <div>13</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="12" class="fas fa-2x fa-couch available"></i>
                <div>12</div>
            </span>
            <span>
                <i id="11" class="fas fa-2x fa-couch available"></i>
                <div>11</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="10" class="fas fa-2x fa-couch available"></i>
                <div>10</div>
            </span>
            <span>
                <i id="9" class="fas fa-2x fa-couch available"></i>
                <div>9</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="8" class="fas fa-2x fa-couch available"></i>
                <div>8</div>
            </span>
            <span>
                <i id="7" class="fas fa-2x fa-couch available"></i>
                <div>7</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="6" class="fas fa-2x fa-couch available"></i>
                <div>6</div>
            </span>
            <span>
                <i id="5" class="fas fa-2x fa-couch available"></i>
                <div>5</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="4" class="fas fa-2x fa-couch available"></i>
                <div>4</div>
            </span>
            <span>
                <i id="3" class="fas fa-2x fa-couch available"></i>
                <div>3</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="2" class="fas fa-2x fa-couch available"></i>
                <div>2</div>
            </span>
            <span>
                <i id="1" class="fas fa-2x fa-couch available"></i>
                <div>1</div>
            </span>
        </div>
        <div class="ui grid">
            <h3>Frente del Autobus</h3>
        </div>
    </div>
    <div class="none"></div>
    <div class="seats second-floor">
        <div class="ui grid">
            <h3>Parte de abajo</h3>
        </div>
        <div class="ui grid">
            <span>
                <i id="61" class="fas fa-2x fa-couch available"></i>
                <div>61</div>
            </span>
            <span>
                <i id="60" class="fas fa-2x fa-couch available"></i>
                <div>60</div>
            </span>
            <span class="hall"></span>
            <i class="fas fa-2x fa-couch" style="color:transparent;"></i>
            <i class="fas fa-2x fa-couch" style="color:transparent;"></i>
        </div>
        <div class="ui grid">
            <span>
                <i id="59" class="fas fa-2x fa-couch available"></i>
                <div>59</div>
            </span>
            <span>
                <i id="58" class="fas fa-2x fa-couch available"></i>
                <div>58</div>
            </span>
            <span class="hall"></span>
            <i class="fas fa-2x fa-couch" style="color:transparent;"></i>
            <i class="fas fa-2x fa-couch" style="color:transparent;"></i>
        </div>
        <div class="ui grid">
            <span>
                <i id="57" class="fas fa-2x fa-couch available"></i>
                <div>57</div>
            </span>
            <span>
                <i id="56" class="fas fa-2x fa-couch available"></i>
                <div>56</div>
            </span>
            <span class="hall"></span>
            <i class="fas fa-2x fa-couch" style="color:transparent;"></i>
            <i class="fas fa-2x fa-couch" style="color:transparent;"></i>
        </div>
        <div class="ui grid">
            <span>
                <i id="55" class="fas fa-2x fa-couch available"></i>
                <div>55</div>
            </span>
            <span>
                <i id="54" class="fas fa-2x fa-couch available"></i>
                <div>54</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="53" class="fas fa-2x fa-couch available"></i>
                <div>53</div>
            </span>
            <span>
                <i id="52" class="fas fa-2x fa-couch available"></i>
                <div>52</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="51" class="fas fa-2x fa-couch available"></i>
                <div>51</div>
            </span>
            <span>
                <i id="50" class="fas fa-2x fa-couch available"></i>
                <div>50</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="49" class="fas fa-2x fa-couch available"></i>
                <div>49</div>
            </span>
            <span>
                <i id="48" class="fas fa-2x fa-couch available"></i>
                <div>48</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="47" class="fas fa-2x fa-couch available"></i>
                <div>47</div>
            </span>
            <span>
                <i id="46" class="fas fa-2x fa-couch available"></i>
                <div>46</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="45" class="fas fa-2x fa-couch available"></i>
                <div>45</div>
            </span>
            <span>
                <i id="44" class="fas fa-2x fa-couch available"></i>
                <div>44</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="43" class="fas fa-2x fa-couch available"></i>
                <div>43</div>
            </span>
            <span>
                <i id="42" class="fas fa-2x fa-couch available"></i>
                <div>42</div>
            </span>
            <span class="hall"></span>
            <span>
                <i id="41" class="fas fa-2x fa-couch available"></i>
                <div>41</div>
            </span>
            <span>
                <i id="40" class="fas fa-2x fa-couch available"></i>
                <div>40</div>
            </span>
        </div>
        <div class="ui grid">
            <span>
                <i id="39" class="fas fa-2x fa-couch available"></i>
                <div>39</div>
            </span>
            <span class="hall"></span>
            <span class="hall"></span>
            <span class="hall"></span>
            <i class="fas fa-2x fa-couch" style="color:transparent;"></i>
            <i class="fas fa-2x fa-couch" style="color:transparent;"></i>
            {{-- <i class="fas fa-2x fa-couch" style="color:transparent;"></i> --}}
        </div>
        <div class="ui grid">
            <h3>Frente del Autobus</h3>
        </div>
    </div>
</div>
