<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Admin Roles
        </h4>
    </div>

    <div class="ui container page-description">
        <h4>
            Todos los roles en el sistema
        </h4>
        <p>
            Usa esta seccion para agregar o modificar roles
        </p>
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <div class="ui text-right">
            <a href="{{ route('roles.create') }}">
                <i class="fas fa-2x fa-plus-square"></i>
            </a>
        </div>
        <table class="ui blue table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th class="right aligned">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($roles as $key => $role)
                    <tr>
                        <td>{{ $role->type }}</td>
                        <td class="right aligned">
                            <a href="{{ route('roles.edit', ['role' => $role->id]) }}">
                                <i class="fas fa-2x fa-eye"></i>
                            </a>
                            <a href="#!" id="modal{{ $role->id }}" onclick="openModal({{ $role->id }})">
                                <i class="fas fa-2x fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10" class="ui message yellow">No hay roles en el sistema</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="ui container text text-center">
        {!! $roles->links('layouts.pagination') !!}
    </div>

    @foreach ($roles as $key => $role)
        <div class="ui mini modal" id="{{ $role->id }}">
            <div class="header">¿Borrar Rol?</div>
            <div class="content">
                <p>
                    {{ $role->type }}
                </p>
                <p>
                    <i class="ui yellow">* ¡Recuerda que no puedes borrar un rol que tenga asignados usuarios!</i>
                </p>
            </div>
            <div class="actions">
                <form action="{{ route('roles.destroy', ['role'=>$role]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">

            $(document).ready( function() {
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
