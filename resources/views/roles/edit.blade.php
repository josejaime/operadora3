<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Editar Rol
        </h4>
    </div>

    <div class="ui container page-description">
        {{-- <h4>
            Todos los usuarios en el sistema
        </h4> --}}
        <p>
            Usa esta seccion para editar el rol
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('roles.update', ['role' => $role->id]) }}">
            @csrf
            @method('PUT')
            <div class="field">
                <label>Nombre del Rol</label>
                <input type="text" name="type" placeholder="Nombre" value="{{ $role->type }}">
            </div>
            <button class="ui button yellow" type="submit">Actualizar</button>
        </form>

    </div>

</x-app-layout>
