<x-app-layout>

    <div class="subtitle-header">
        <h4>
            Agregar Rol
        </h4>
    </div>

    <div class="ui container page-description">
        {{-- <h4>
            Todos los usuarios en el sistema
        </h4> --}}
        <p>
            Usa esta seccion para agregar un rol
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('roles.store') }}">
            @csrf
            <div class="field">
                <label>Nombre del Rol</label>
                <input type="text" name="type" placeholder="Nombre" value="{{ old('name') }}">
            </div>
            <button class="ui button purple" type="submit">Guardar</button>
        </form>

    </div>

</x-app-layout>
