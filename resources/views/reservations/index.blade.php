<x-app-layout>
    <div class="subtitle-header">
        <h4>
            Reservaciones
        </h4>
    </div>

    <div class="ui container page-description">
        <h4>
            ¿Qué te gustaría hacer?
        </h4>
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <h2>Reservaciones</h2>
        <div class="grid-container">
            <a href="{{ route('reservations.select.tour') }}" class="action ui button purple">
                Crear nueva
            </a>
            <a href="{{ route('reservations.select.tour.review') }}" class="action ui button purple">
                Revisar tour
            </a>
            <a href="#!" class="action ui button purple">
                Confirma
            </a>
            <a href="#!" class="action ui button purple">
                Mis reservaciones
            </a>
        </div>
        <h2>Reportes</h2>
        <div class="grid-container">
            <a href="#!" class="action ui button violet">
                Recolección
            </a>
        </div>
    </div>
</x-app-layout>
