<x-app-layout>
    <div class="subtitle-header">
        <h4>
            Tus Reservaciones
        </h4>
    </div>

    <div class="ui container page-description">
        @include('layouts.messages')
    </div>

    <div class="ui container main message blue">
        Reservas para:
        <h3>
            {{ $tour->name }} <br>
            <small>
                {{ \Carbon\Carbon::createFromTimeStamp(strtotime($date))->translatedFormat('d F Y') }} --
                {{ $departure->hour }} Horas</small>
        </h3>
    </div>

    <div class="ui container main" style="overflow-x: scroll;">
        <a class="ui button primary" href="{{ route('reservations.create', ['date' => $date, 'tour_id' => $tour->id ]) }}">Crear Otra</a>
        <a class="ui button yellow" href="{{ route('reservations.tour.review', ['date' => $date, 'departure_id' => $departure->id, 'pdf' => true ]) }}">PDF</a>
        <table class="ui blue table">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Nombre</th>
                    <th>Tour</th>
                    <th>Hotel</th>
                    <th>A</th>
                    <th>N</th>
                    <th>I</th>
                    <th>Total</th>
                    <th class="right aligned">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($reservations as $key => $reservation)
                    <tr>
                        <td>{{ $reservation->folio }}</td>
                        <td>{{ $reservation->client }}</td>
                        <td>{{ $tour->name }}</td>
                        <td>{{ $reservation->hotel->name }}</td>
                        <td>{{ $reservation->adults }}</td>
                        <td>{{ $reservation->kids }}</td>
                        <td>{{ $reservation->elders }}</td>
                        <td>{{ $reservation->total }}</td>
                        <td class="right aligned" style="pointer-events: all;">
                            {{-- <a href="{{ route('users.show', ['reservation' => $reservation]) }}"> --}}
                            <a href="{{ route('reservations.show', ['reservation' => $reservation]) }}">
                                <i class="fas fa-2x fa-eye"></i>
                            </a>
                            <a href="#!" id="modal{{ $reservation->id }}" onclick="openModal({{ $reservation->id }})">
                                <i class="fas fa-2x fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10" class="ui message yellow">
                            No hay reservaciones para este tour
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>

    @foreach ($reservations as $key => $reservation)
        <div class="ui mini modal" id="{{ $reservation->id }}">
            <div class="header">¿Borrar Reservación?</div>
            <div class="content">
                <p>
                    {{ $reservation->folio }}
                </p>
                <p>
                    <b>Cliente</b><br>
                    {{ $reservation->client }}
                </p>
                <p>
                    <small>
                        <i>*Una vez borrada no se puede recuperar</i>
                    </small>
                </p>
            </div>
            <div class="actions">
                <form action="{{ route('reservations.destroy', ['reservation' => $reservation]) }}" method="post" style="display: inline-block;">
                    @csrf
                    @method('delete')
                    <input type="submit" class="ui red button" value="¿Borrar?">
                </form>
                <div class="ui cancel purple button">Cancelar</div>
            </div>
        </div>
    @endforeach

    @push('scripts')
        <script type="text/javascript">
            $(document).ready(function(){
                $('.ui.mini.modal')
                .modal()
                ;
            });
            function openModal( id ){
                $('#'+id+'.ui.modal')
                  .modal('show')
                ;
            }
        </script>
    @endpush

</x-app-layout>
