<x-app-layout>
    {{-- <div class="subtitle-header">
        <h4>
            Reservaciones
        </h4>
    </div> --}}
    <div class="ui steps">
        <a class="active step">
            <div class="content">
                <div class="title">Tour</div>
                <div class="description">Selecciona un tour</div>
            </div>
        </a>
        <a class="step">
            <div class="content">
                <div class="title">LLena</div>
                <div class="description">Completa la reserva</div>
            </div>
        </a>
    </div>

    <div class="ui container page-description">
        <p>
            Usa esta seccion para agregar una reserva
        </p>
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <h4>Selecciona una fecha</h4>
        <form id="form_send" class="ui form" method="get" action="{{ route('reservations.create') }}">
            <div class="field">
                <input type="text" class="datepicker" name="date">
            </div>
            <input type="hidden" name="tour_id" value="" id="tour_id">
            {{-- <button class="ui button purple" type="submit">Guardar</button> --}}
        </form>
    </div>

    @forelse ($companies as $key => $company)
        <div class="ui container main">
            <h3>{{ $company->name }}</h3>
            <div class="grid-container">
                @forelse ($company->tours as $key => $tour)

                    @if ( $tour->active && $tour->hasEnabledDepartures() )
                        <a href="#!" class="action ui button yellow" id="{{ $tour->id }}">
                            {{ $tour->name }}
                        </a>
                    @else
                        <a href="#!" class="action ui button disabled">
                            {{ $tour->name }}
                        </a>
                    @endif

                @empty
                    <div class="ui message warning">
                        <p>No hay tours en el sistema</p>
                    </div>
                @endforelse
            </div>
            <div class="spacer"></div>
            <div class="ui">
                <p><i>* Los desactivados no tienen horarios de salida o están desactivados de manera general</i></p>
            </div>
        </div>
    @empty
        <div class="ui container main">
            <div class="ui message warning">
                <p>No hay tours en el sistema</p>
            </div>
        </div>
    @endforelse

    @forelse ($fastes as $key => $fast)
        <div class="ui container main">
            <h3>FAST TRACK</h3>
            <div class="grid-container">
                @if ( $fast->active && $fast->hasEnabledDepartures() )
                    <a href="#!" class="action ui button purple" id="{{ $fast->id }}">
                        {{ $fast->name }}
                    </a>
                @else
                    <a href="#!" class="action ui button disabled">
                        {{ $fast->name }}
                    </a>
                @endif
            </div>
            <div class="spacer"></div>
            <div class="ui">
                <p><i>* Los desactivados no tienen horarios de salida o están desactivados de manera general</i></p>
            </div>
        </div>
    @empty
        <div class="ui container main">
            <div class="ui message warning">
                <p>No hay tours en el sistema</p>
            </div>
        </div>
    @endforelse

    @push('scripts')
        <link rel="stylesheet" href="{{ asset('lib/calendar/calendar.css') }}">
        <script src="{{ asset('lib/materialize/js/materialize.min.js') }}" charset="utf-8"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('select').formSelect();
                $('.datepicker').datepicker({
                    format: 'yyyy-mm-dd',
                    defaultDate: new Date(),
                    setDefaultDate: true,
                    minDate: new Date(),
                    i18n: {
                            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                            weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                            weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                            weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"]
                        }
                });

                $('.action').on('click', function() {
                    console.log('tour_id = ' + $(this).attr('id'));
                    $('#tour_id').val( $(this).attr('id') );
                    $('#form_send').submit();
                });
            });
        </script>
    @endpush

</x-app-layout>
