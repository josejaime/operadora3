<x-app-layout>
    {{-- <div class="subtitle-header">
        <h4>
            Agregar Hotel
        </h4>
    </div> --}}
    <div class="ui steps">
        <a class="step">
            <div class="content">
                <div class="title">Tour</div>
                <div class="description">Seleccionado</div>
            </div>
        </a>
        <a class="active step">
            <div class="content">
                <div class="title">LLena</div>
                <div class="description">Completa la reserva</div>
            </div>
        </a>
    </div>

    <div class="ui container page-description">
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main message blue">
        <p>
            {{-- @php
                setLocale(LC_TIME, 'es');
            @endphp --}}
            {{-- Reserva para: {{\Carbon\Carbon::createFromTimeStamp(strtotime($date))->diffForHumans()}} --}}
            Reserva para: <b>{{\Carbon\Carbon::createFromTimeStamp(strtotime($date))->translatedFormat('d F Y') }}</b>
        </p>
        <h3 style="margin-top:5px">{{ $tour->name }}</h3>
    </div>

    <div class="ui container main">

        <form class="ui form"
                method="{{ $tour->company->id == 1 ? 'post' : 'get' }}"
                action="{{ $tour->company->id == 1 ? route('reservations.store') :  route('reservations.create.select.seats') }}">
            @csrf
            <div class="field">
                <div class="field">
                    <button class="ui button purple" type="button" id="fill">Llenado rápido</button>
                </div>
                <div class="two fields">
                    <div class="field">
                        <label>Apellido</label>
                        <input type="text" name="last_name" placeholder="Apellido(s)" value="{{ old('last_name') }}" class="validate">
                    </div>
                    <div class="field">
                        <label>Nombre</label>
                        <input type="text" name="first_name" placeholder="Nombre(s)" value="{{ old('first_name') }}" class="validate">
                    </div>
                </div>
            </div>
            <input type="hidden" name="date" value="{{ \Carbon\Carbon::createFromTimeStamp(strtotime($date))->toDateString() }}">
            <div class="field">
                <label>Email</label>
                <input type="email" name="email" placeholder="Email del cliente" value="{{ old('email') }}">
            </div>
            <div class="field">
                <label>Teléfono</label>
                <input type="text" name="telephone" placeholder="Teléfono del cliente" value="{{ old('telephone') }}">
            </div>
            <div class="ui segment">
                <div class="field">
                    <div class="ui toggle checkbox">
                        <input type="checkbox" name="send_sms" tabindex="0" class="hidden">
                        <label>¿Enviar mensaje SMS?</label>
                    </div>
                </div>
            </div>
            <div class="six fields">
                <div class="field">
                    <label>Adultos</label>
                    <input type="number" name="adults" id="adults" min=0 placeholder="Adultos" value="{{ old('adults') ? old('adults') : 0 }}">
                </div>
                <div class="field">
                    <label>Precio</label>
                    <input type="number" name="cost_adults" id="cost_adults" readonly placeholder="Precio adultos" value="{{ $tour->cost_adults }}">
                </div>
                <div class="field">
                    <label>Niños</label>
                    <input type="number" name="kids" id="kids" min=0 placeholder="Niños" value="{{ old('kids') ? old('kids') : 0 }}">
                </div>
                <div class="field">
                    <label>Precio</label>
                    <input type="number" name="cost_kids" id="cost_kids" readonly placeholder="Precio Niños" value="{{ $tour->cost_kids }}">
                </div>
                <div class="field">
                    <label>Insen</label>
                    <input type="number" name="insen" id="insen" min=0 placeholder="Insen" value="{{ old('elders') ? old('elders') : 0 }}">
                </div>
                <div class="field">
                    <label>Precio</label>
                    <input type="number" name="cost_elders" id="cost_elders" readonly placeholder="Precio INSEN" value="{{ $tour->cost_elders }}">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Hotel</label>
                    <select class="ui fluid dropdown" name="hotel_id">
                        @forelse ($hotels as $key => $hotel)
                            <option value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                        @empty
                            <option class="ui message yellow" value="null">No hay hoteles por el momento</option>
                        @endforelse
                    </select>
                </div>
                <div class="field">
                    <label>Habitación</label>
                    <input type="number" name="room" placeholder="Num habitación" value="{{ old('room') ? old('room') : 0 }}" class="validate">
                </div>
            </div>
            <div class="field">
                <label>Horario de salida</label>
                <select class="ui fluid dropdown" name="departure_id">
                    @forelse ($tour->departures->sortBy('hour') as $key => $departure)
                        <option value="{{ $departure->id }}">{{ $departure->hour }}</option>
                    @empty
                        <option class="ui message yellow" value="null">No hay salidas por el momento</option>
                    @endforelse
                </select>
            </div>
            <div class="field">
                <label>Método de Pago</label>
                <select class="ui fluid dropdown" name="method_id" id="payment_method">
                    @foreach ($methods as $key => $method)
                        <option value="{{ $method->id }}">{{ $method->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="field field-card disabled">
                <label>Últimos dígitos de la tarjeta</label>
                <input type="number" name="card" id="card" disabled placeholder="Últimos dígitos de la tarjeta">
            </div>
            <div class="field field-citypass disabled">
                <label>TOTAL PASS</label>
                <input type="text" name="citypass" id="citypass" disabled placeholder="Zacatecas Total Pass">
            </div>
            <div class="three fields">
                <div class="field">
                    <label>Pago</label>
                    <input type="number" name="actual_pay" id="actual_pay" class="validate">
                </div>
                <div class="field">
                    <label>Total</label>
                    <input type="number" name="total" id="total" readonly>
                </div>
                <div class="field">
                    <label>Comisión</label>
                    <input type="number" name="commission" readonly disabled id="commission">
                </div>
            </div>
            <div class="field">
                <label>Cambio</label>
                <input type="number" name="change" readonly disabled id="change">
            </div>
            <div class="field">
                <label>Notas extra</label>
                <textarea rows="2" name="comments"></textarea>
            </div>

            {{-- <input type="hidden" name="seat-{{$departure->type}}[1]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[2]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[3]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[4]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[5]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[6]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[7]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[8]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[9]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[10]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[11]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[12]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[13]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[14]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[15]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[16]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[17]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[18]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[19]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[20]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[21]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[22]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[23]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[24]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[25]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[26]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[27]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[28]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[29]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[30]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[31]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[32]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[33]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[34]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[35]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[36]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[37]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[38]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[39]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[40]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[41]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[42]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[43]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[44]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[45]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[46]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[47]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[48]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[49]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[50]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[51]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[52]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[53]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[54]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[55]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[56]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[57]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[58]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[59]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[60]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[61]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[62]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[63]" value="available"> --}}

            {{-- @if ($departure->type != null)
                @if ($departure->type == 1)
                    @include('departures.bus_layout')
                @else
                    @include('departures.bus_small')
                @endif
            @endif --}}

            <button class="ui button green" id="validate" type="submit">Crear Reserva</button>
        </form>
        {{-- <button id="validate">Show Snackbar</button> --}}

    </div>

    @push('scripts')
        <script type="text/javascript">
            var counter = 0;
            var total_tickets = 0;

            $( document ).ready(function() {
                $('.ui.checkbox')
                  .checkbox()
                ;
                //===============================================
                //== DO SOME INIT CHECKS
                //===============================================
                if($('#payment_method').val() == 'tarjeta') {
                    $('.field-card').removeClass('disabled');
                    $('.field-citypass').addClass('disabled');
                    $('#card').prop('disabled', false);
                    $('#citypass').prop('disabled', true);
                } else if ($('#payment_method').val() == 'citypass') {
                    $('.field-citypass').removeClass('disabled');
                    $('.field-card').addClass('disabled');
                    $('#card').prop('disabled', true);
                    $('#citypass').prop('disabled', false);
                }

                //===============================================
                //== Payment Method On Change
                //===============================================
                $('#payment_method').on('change', function() {
                    console.log("Changing "+$(this).val());
                    if ( $(this).val() ==  "2" ) {
                        $('.field-card').removeClass('disabled');
                        $('.field-citypass').addClass('disabled');
                        $('#card').prop('disabled', false);
                        $('#card').addClass('validate');
                        $('#citypass').prop('disabled', true);
                        $('#citypass').val('');
                        $('#citypass').removeClass('validate');
                    } else if ( $(this).val() ==  "3" ) {
                        $('.field-citypass').removeClass('disabled');
                        $('.field-card').addClass('disabled');
                        $('#card').prop('disabled', true);
                        $('#card').val('');
                        $('#citypass').prop('disabled', false);
                        $('#citypass').addClass('validate');
                        $('#card').removeClass('validate');
                    } else if ( $(this).val() ==  "cortesia" ) {
                        $("#total").val(0);
                        $("#actual_pay").val(0);
                        $("#commission").val(0);
                        $('#card').val('');
                        $('#citypass').val('');
                        $('.field-card').addClass('disabled');
                        $('.field-citypass').addClass('disabled');
                        $('.field-card input').removeClass('validate');
                        $('.field-citypass input').removeClass('validate');
                    }
                    else {
                        $('#card').val('');
                        $('#citypass').val('');
                        $('.field-card').addClass('disabled');
                        $('.field-citypass').addClass('disabled');
                        $('.field-card input').removeClass('validate');
                        $('.field-citypass input').removeClass('validate');
                    }
                });//

                //===============================================
                //== Function to do the math
                //===============================================
                $('#adults, #kids, #insen, #cost_adults, #cost_kids, #cost_elders, #actual_pay').on('input', function() {
                    var price_adults = $('#cost_adults').val();
                    var price_kids = $('#cost_kids').val();
                    var price_elders = $('#cost_elders').val();

                    @php
                        $commission = Auth::user()->commissions()->where('tour_id', $tour->id)->first();
                    @endphp

                    @if ( $commission )
                        var commission_kids = {{ $commission->kids }};
                        var commission_adults = {{ $commission->adults }};
                        var commission_elders = {{ $commission->elders }};
                    @else
                        var commission_adults = 0;
                        var commission_kids = 0;
                        var commission_elders = 0;
                    @endif

                    var adults = getNumber( $('#adults') ) ? $('#adults').val() : '0';
                    var kids = getNumber( $('#kids') ) ? $('#kids').val() : '0';
                    var insen = getNumber( $('#insen') ) ? $('#insen').val() : '0';

                    console.log('Precios');
                    console.log('A = '+price_adults + ' k = '+price_kids + ' I = '+price_elders);
                    console.log('Numeros');
                    console.log('A = '+adults+' k = '+kids+' I = '+insen);

                    var total = parseInt(price_adults) * parseInt(adults)
                                + parseInt(price_kids) * parseInt(kids)
                                + parseInt(price_elders) * parseInt(insen);

                    var total_commissions = (parseInt(adults) * parseInt(commission_adults)) +
                                + (parseInt(kids) * parseInt(commission_kids)) +
                                + (parseInt(insen) * parseInt(commission_elders));

                    // console.log('A,CA = '+(adults * commission_adults)+
                    //             'k,CK = '+(kids * commission_kids)+
                    //             'I,CI = '+(insen * commission_elders));
                    var actual_pay = getNumber($('#actual_pay')) ? $('#actual_pay').val() : total;

                    var change = parseInt(actual_pay) - parseInt(total);


                    $('#total').val(total);
                    change > 0 ? $('#change').val(change) : $('#change').val('0');
                    $('#commission').val(total_commissions);

                    //==========================================
                    //== Lets fill the tickets if needed
                    //==========================================
                    total_tickets = parseInt(adults) + parseInt(kids) + parseInt(insen);
                });

                //===============================================
                //== Function to quick fill the form
                //===============================================
                $("#fill").on('click', function() {
                    console.log("Llenado rápido");
                    $( "input[name='first_name']" ).val('LLENADO RAPIDO');
                    $( "input[name='last_name']" ).val('{{ Auth::user()->username }}');
                    $( "input[name='email']" ).val('reservaciones@operadorazacatecas.mx');
                    $( "input[name='telephone']" ).val('4921246452');

                    $( "input[name='room']" ).val('101');
                    $( "textarea" ).val('Reserva llenada automaticamente desde la app por {{ Auth::user()->username }}');
                });

                $("#validate").on('click', function( event ) {
                    event.preventDefault();
                    $('.field').removeClass('error');
                    var ok = true;
                    $('input.validate').each( function() {
                        if( ! $(this).val() ) {
                            console.log("error");
                            ok = false;
                            $(this).parent('.field').addClass('error');
                        }
                    });
                    if(!ok) {
                        showSnackBar("Debes completar la forma antes de continuar");
                    }
                    else {
                        $('form').submit();
                    }
                });

                //===============================================
                //== Function to select the seats if neccesary
                //===============================================
                $(".seats span").on('click', function(){
                    var id =  $(this).children('svg').attr('id');
                    console.log("Tratando de asignar asiento "+id);
                    //if (counter < total_tickets && !$(this).hasClass('selection') ) {
                    //if (counter < total_tickets) {
                        if ( counter < total_tickets && !$(this).children('svg').hasClass('selection') ) {

                            // console.log("ID = "+$(this).attr('id'));
                            $(this).children('.fa-2x').toggleClass("selection");
                            $("input[name='seat-{{$departure->type}}["+id+"]']").val("selection");
                            counter = counter + 1;
                            //remainingSeats = remainingSeats - 1;
                        }
                        else if ( $(this).children('svg').hasClass("selection") ) {
                            $(this).children('svg').toggleClass("selection");
                            $("input[name='seat-{{$departure->type}}["+id+"]']").val("available");
                            counter = counter - 1;
                            //remainingSeats = remainingSeats + 1;
                        }
                    //}
                    // else {
                    //     console.log('No tienes boletos para asignar');
                    // }
                    /*$("#ticket-label").html((total_tickets - counter));
                    console.log("counter "+counter);
                    console.log("Remaining Seats on click "+remainingSeats);*/
                });

            });
            function getNumber( element ) {
                if( element.val() ) {
                    return true;
                }
                else {
                    return false;
                }
            }
        </script>
    @endpush

</x-app-layout>
