<x-app-layout>
    {{-- <div class="subtitle-header">
        <h4>
            Agregar Hotel
        </h4>
    </div> --}}
    <div class="ui steps">
        <a class="step">
            <div class="content">
                <div class="title">Tour</div>
                <div class="description">Seleccionado</div>
            </div>
        </a>
        <a class="step">
            <div class="content">
                <div class="title">LLena</div>
                <div class="description">Completa la reserva</div>
            </div>
        </a>
        <a class="active step">
            <div class="content">
                <div class="title">Asientos</div>
                <div class="description">Selecciona los lugares</div>
            </div>
        </a>
    </div>

    <div class="ui container page-description">
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main message blue">
        <p>
            {{-- @php
                setLocale(LC_TIME, 'es');
            @endphp --}}
            {{-- Reserva para: {{\Carbon\Carbon::createFromTimeStamp(strtotime($date))->diffForHumans()}} --}}
            Reserva para: <b>{{\Carbon\Carbon::createFromTimeStamp(strtotime($date))->translatedFormat('d F Y') }}</b>
        </p>
        <h3 style="margin-top:5px">{{ $tour->name }}</h3>
    </div>

    <div class="ui container main">
        <div class="ui grid">
            <div>
                <div class="spacer"></div>
                <h3>Datos generales</h3>
            </div>
        </div>
        <div class="ui stackable grid">
            <div class="eight wide column">
                <div class="ui celled list">
                    <div class="item">
                        <div class="content">
                            <div class="header">Cliente</div>
                            {{ $reservation->last_name }} {{ $reservation->first_name }}
                        </div>
                    </div>
                    <div class="item">
                        <div class="content">
                            <div class="header">Email</div>
                            {{ $reservation->email ? $reservation->email : 'N/A'  }}
                        </div>
                    </div>
                    <div class="item">
                        <div class="content">
                            <div class="header">Teléfono</div>
                            {{ $reservation->telephone ? $reservation->telephone : 'N/A' }}
                        </div>
                    </div>
                    {{-- <div class="item">
                        <div class="content">
                            <div class="header">send_sms</div>
                            {{ $reservation->send_sms ? 'checked' : '' }}
                        </div>
                    </div> --}}
                    <div class="item">
                        <div class="content">
                            <style media="screen">
                                .item {
                                    padding-top: 5px !important;
                                    padding-bottom: 7px !important;
                                }
                                .parts {
                                    display: grid;
                                    grid-template-columns: 1fr 1fr 1fr;
                                }
                            </style>
                            <div class="parts">
                                <div class="part">
                                    <div>
                                        <b>Adultos</b>
                                    </div>
                                    {{ $reservation->adults ? $reservation->adults : '0'  }}
                                </div>
                                <div class="part">
                                    <div>
                                        <b>Niños</b>
                                    </div>
                                    {{ $reservation->kids ? $reservation->kids : '0'  }}
                                </div>
                                <div class="part">
                                    <div>
                                        <b>Insen</b>
                                    </div>
                                    {{ $reservation->elders ? $reservation->elders : '0'  }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="eight wide column">
                <div class="ui celled list">
                    <div class="item">
                        <div class="content">
                            <div class="header">Hotel</div>
                            {{ $hotel->name  }} <b>{{ $reservation->room  }}</b>
                        </div>
                    </div>
                    {{-- <div class="item">
                        <div class="content">
                            <div class="header">departure_id</div>
                            {{ $reservation->departure_id  }}
                        </div>
                    </div> --}}
                    <div class="item">
                        <div class="content">
                            <div class="header">Método de pago</div>
                            {{ $method->name  }}
                        </div>
                    </div>
                    @if ( $reservation->card )
                        <div class="item">
                            <div class="content">
                                <div class="header">card</div>
                                {{ $reservation->card  }}
                            </div>
                        </div>
                    @endif
                    @if ( $reservation->citypass )
                        <div class="item">
                            <div class="content">
                                <div class="header">citypass</div>
                                {{ $reservation->citypass  }}
                            </div>
                        </div>
                    @endif
                    {{-- <div class="item">
                        <div class="content">
                            <div class="header">Pago</div>
                            {{ $reservation->actual_pay  }}
                        </div>
                    </div> --}}
                    <div class="item">
                        <div class="content">
                            <div class="parts">
                                <div class="part">
                                    <div><b>Total</b></div>
                                    {{ $reservation->total  }}
                                </div>
                                <div class="part">
                                    <div><b>Pago</b></div>
                                    {{ $reservation->actual_pay }}
                                </div>
                                <div class="part">
                                    <div><b>Comisión</b></div>
                                    {{ $reservation->commission ? $reservation->commission : '0' }}
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="item">
                        <div class="content">
                            <div class="header">commission</div>
                            {{ $reservation->commission ? $reservation->commission : '0' }}
                        </div>
                    </div> --}}
                    {{-- <div class="item">
                        <div class="content">
                            <div class="header">change</div>
                            {{ $reservation->change ? $reservation->change : '0'  }}
                        </div>
                    </div> --}}
                    <div class="item">
                        <div class="content">
                            <div class="header">comments</div>
                            {{ $reservation->comments  }}
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end grid -->
    </div><!-- End container -->

    <div class="ui container main">

        <form class="ui form" method="post" action="{{ route('reservations.store') }}">
            @csrf
            <input type="hidden" name="last_name" placeholder="Apellido(s)" value="{{ $reservation->last_name }}">
            <input type="hidden" name="first_name" placeholder="Nombre(s)" value="{{ $reservation->first_name }}">
            <input type="hidden" name="date" value="{{ \Carbon\Carbon::createFromTimeStamp(strtotime($reservation->date))->toDateString() }}">

            <input type="hidden" name="email" placeholder="Email del cliente" value="{{ $reservation->email }}">
            <input type="hidden" name="telephone" placeholder="Teléfono del cliente" value={{ $reservation->telephone }}>
            <input type="hidden" name="send_sms" tabindex="0" class="hidden" {{ $reservation->send_sms ? 'checked' : '' }}>
            <input type="hidden" name="adults" id="adults" min=0 placeholder="Adultos" value="{{ $reservation->adults }}">
            <input type="hidden" name="cost_adults" id="cost_adults" readonly placeholder="Precio adultos" value="{{ $reservation->cost_adults }}">
            <input type="hidden" name="kids" id="kids" min=0 placeholder="Niños" value="{{ $reservation->kids }}">
            <input type="hidden" name="cost_kids" id="cost_kids" readonly placeholder="Precio Niños" value="{{ $reservation->cost_kids }}">
            <input type="hidden" name="insen" id="insen" min=0 placeholder="Insen" value="{{ $reservation->elders }}">
            <input type="hidden" name="cost_elders" id="cost_elders" readonly placeholder="Precio INSEN" value="{{ $reservation->cost_elders }}">
            <input type="hidden" name="hotel_id" value="{{ $reservation->hotel_id }}">
            <input type="hidden" name="room" placeholder="Num habitación" value="{{ $reservation->room }}">
            <input type="hidden" name="departure_id" value="{{ $reservation->departure_id }}">
            <input type="hidden" name="method_id" value="{{ $reservation->method_id }}">
            <input type="hidden" name="card" value="{{ $reservation->card }}">
            <input type="hidden" name="citypass" id="citypass" value="{{ $reservation->citypass }}">
            <input type="hidden" name="actual_pay" id="actual_pay" value="{{ $reservation->actual_pay }}">
            <input type="hidden" name="total" id="total" value="{{ $reservation->total }}">
            <input type="hidden" name="commission" id="commission" value="{{ $reservation->commission }}">
            <input type="hidden" name="change" id="change" value="{{ $reservation->change }}">
            <input type="hidden" name="comments" value="{{ $reservation->comments }}">

            <input type="hidden" name="seat-{{$departure->type}}[1]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[2]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[3]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[4]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[5]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[6]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[7]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[8]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[9]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[10]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[11]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[12]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[13]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[14]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[15]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[16]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[17]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[18]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[19]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[20]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[21]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[22]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[23]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[24]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[25]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[26]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[27]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[28]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[29]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[30]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[31]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[32]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[33]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[34]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[35]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[36]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[37]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[38]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[39]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[40]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[41]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[42]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[43]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[44]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[45]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[46]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[47]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[48]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[49]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[50]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[51]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[52]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[53]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[54]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[55]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[56]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[57]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[58]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[59]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[60]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[61]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[62]" value="available">
            <input type="hidden" name="seat-{{$departure->type}}[63]" value="available">

            @if ($departure->type != null)
                {{-- BUS LAYOUT --}}
                @if ($departure->type == 1)
                    @include('departures.bus_big')
                @else
                    @include('departures.bus_small')
                @endif
                {{-- / BUS LAYOUT --}}
            @endif
            <div class="spacer"></div>
            <button class="ui button green" id="validate" type="submit">Crear Reserva</button>
        </form>
        {{-- <button id="validate">Show Snackbar</button> --}}

    </div>

    @push('scripts')
        <script type="text/javascript">
            var counter = 0;
            @php
                $total_tickets = $reservation->adults + $reservation->kids + $reservation->elders;
            @endphp
            var total_tickets = {{ $total_tickets }};

            $( document ).ready(function() {
                /// Se debe realizar una verificación de la cantidad de
                /// Tickets disponibles justo antes de esto
                //===============================================
                //== Function to select the seats
                //===============================================
                $(".seats span").on('click', function(){
                    var id =  $(this).children('svg').attr('id');
                    console.log("Tratando de asignar asiento "+id);
                    //if (counter < total_tickets && !$(this).hasClass('selection') ) {
                    //if (counter < total_tickets) {
                        if ( counter < total_tickets && !$(this).children('svg').hasClass('selection') ) {

                            console.log("ENTER IF ID = "+$(this).attr('id'));
                            $(this).children('.fa-2x').toggleClass("selection");
                            $("input[name='seat-{{$departure->type}}["+id+"]']").val("selection");
                            counter = counter + 1;
                            //remainingSeats = remainingSeats - 1;
                        }
                        else if ( $(this).children('svg').hasClass("selection") ) {
                            console.log("ENTER ELSE ID = "+$(this).attr('id'));
                            $(this).children('svg').toggleClass("selection");
                            $("input[name='seat-{{$departure->type}}["+id+"]']").val("available");
                            counter = counter - 1;
                            //remainingSeats = remainingSeats + 1;
                        }
                        else {
                            console.log('No tienes boletos para asignar');
                        }
                    /*$("#ticket-label").html((total_tickets - counter));
                    console.log("counter "+counter);
                    console.log("Remaining Seats on click "+remainingSeats);*/
                });

            });
            function getNumber( element ) {
                if( element.val() ) {
                    return true;
                }
                else {
                    return false;
                }
            }
        </script>
    @endpush

</x-app-layout>
