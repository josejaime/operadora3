<x-app-layout>
    <div class="subtitle-header">
        <h4>
            Reservaciones para una fecha especifica
        </h4>
    </div>

    <div class="ui container page-description">
        @include('layouts.messages')
    </div>

    <div class="ui container main">
        <h4>Selecciona una fecha</h4>
        <form id="form_send" class="ui form" method="get" action="{{ route('reservations.tour.review') }}">
            <div class="field">
                <input type="text" class="datepicker" name="date">
            </div>
            <input type="hidden" name="departure_id" value="" id="departure_id">
            {{-- <button class="ui button purple" type="submit">Guardar</button> --}}
        </form>
    </div>

    @forelse ($companies as $key => $company)
        <div class="ui container main">
            <h3>{{ $company->name }}</h3>
            <div class="grid-container">
                @forelse ($company->tours as $key => $tour)
                    {{-- <a href="#!" class="action ui button yellow" id="{{ $tour->id }}">
                        {{ $tour->name }}
                    </a> --}}
                    <div class="ui form button teal" method="get" id="jhfsd" action="{{ route('users.store') }}" autocomplete="off">
                        @csrf
                        <div class="field" id="gggh">
                            <label style="font-size:1.3rem;">{{ $tour->name }}</label>
                            <div class="spacer"></div>
                            @if (count($tour->departures) > 0)
                                <select class="ui fluid dropdown" id="departure_id" name="departure_id">
                                    @foreach ($tour->departures as $key => $departure)
                                        <option value="{{ $departure->id }}">{{ $departure->hour }}</option>
                                    @endforeach
                                </select>
                            @else
                                <p>No hay horarios para mostrar aquí</p>
                            @endif
                        </div>
                        @if (count($tour->departures) > 0)
                            <button class="ui button black" id="{{ $tour->departures->first()->id}}">Ver horario</button>
                        @endif
                    </div>
                @empty
                    <div class="ui message warning">
                        <p>No hay tours en el sistema</p>
                    </div>
                @endforelse
            </div>
        </div>
    @empty
        <div class="ui container main">
            <div class="ui message warning">
                <p>No hay tours en el sistema</p>
            </div>
        </div>
    @endforelse

    @push('scripts')
        <link rel="stylesheet" href="{{ asset('lib/calendar/calendar.css') }}">
        <script src="{{ asset('lib/materialize/js/materialize.min.js') }}" charset="utf-8"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                //$('.form select').formSelect();
                $('.datepicker').datepicker({
                    format: 'yyyy-mm-dd',
                    defaultDate: new Date(),
                    setDefaultDate: true,
                    //minDate: new Date(),
                    i18n: {
                            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                            weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                            weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                            weekdaysAbbrev: ["D","L", "M", "M", "J", "V", "S"]
                        }
                });

                $('select').on('change', function() {
                    console.log($(this).val());
                    var departure_id = $(this).val();
                    console.log($(this).parent('.selection').parent('.field').parent('.form').children('.button').attr('id', departure_id));
                });

                $('.button.black').on('click', function() {
                    $('#form_send #departure_id').val( $(this).attr('id') );
                    //$('#tour_id').val( $(this).attr('id') );
                    $('#form_send').submit();
                });
            });
        </script>
    @endpush

</x-app-layout>
