<x-app-layout>
    <div class="subtitle-header">
        <h4>
            Revisa la reservación
        </h4>
    </div>

    <div class="ui container page-description">
        @if ($errors->any())
            <div class="ui message warning">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="ui container main message blue">
        <p>
            {{-- @php
                setLocale(LC_TIME, 'es');
            @endphp --}}
            Reserva para: <b>{{\Carbon\Carbon::createFromTimeStamp(strtotime($reservation->date))->translatedFormat('d F Y') }}</b>
        </p>
        <h3 style="margin-top:5px">{{ $reservation->departure->tour->name }}</h3>
    </div>

    <div class="ui container main">
        <form class="ui form" method="get">
            @csrf
            <div class="field">
                <label>Nombre</label>
                <input type="text" name="first_name" placeholder="Nombre(s)" value="{{ $reservation->client }}" class="validate">
            </div>
            <div class="field">
                <label>Email</label>
                <input type="email" name="email" placeholder="Email del cliente" value="{{ $reservation->email }}">
            </div>
            <div class="field">
                <label>Teléfono</label>
                <input type="text" name="telephone" placeholder="Teléfono del cliente" value={{ $reservation->telephone }}>
            </div>
            <div class="six fields">
                <div class="field">
                    <label>Adultos</label>
                    <input type="number" name="adults" id="adults" min=0 placeholder="Adultos" value="{{ $reservation->adults ? $reservation->adults : 0 }}">
                </div>
                <div class="field">
                    <label>Precio</label>
                    <input type="number" name="cost_adults" id="cost_adults" readonly placeholder="Precio adultos" value="{{ $reservation->getTour()->cost_adults }}">
                </div>
                <div class="field">
                    <label>Niños</label>
                    <input type="number" name="kids" id="kids" min=0 placeholder="Niños" value="{{ $reservation->kids ? $reservation->kids : 0 }}">
                </div>
                <div class="field">
                    <label>Precio</label>
                    <input type="number" name="cost_kids" id="cost_kids" readonly placeholder="Precio Niños" value="{{ $reservation->getTour()->cost_kids }}">
                </div>
                <div class="field">
                    <label>Insen</label>
                    <input type="number" name="insen" id="insen" min=0 placeholder="Insen" value="{{ $reservation->elders ? $reservation->elders : 0 }}">
                </div>
                <div class="field">
                    <label>Precio</label>
                    <input type="number" name="cost_elders" id="cost_elders" readonly placeholder="Precio INSEN" value="{{ $reservation->getTour()->cost_elders }}">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Hotel</label>
                    <select class="ui fluid dropdown" name="hotel_id">
                        <option value="{{ $reservation->hotel->id }}">{{ $reservation->hotel->name }}</option>
                    </select>
                </div>
                <div class="field">
                    <label>Habitación</label>
                    <input type="number" name="room" placeholder="Num habitación" value="{{ $reservation->room ? $reservation->room : 0 }}" class="validate">
                </div>
            </div>
            <div class="field">
                <label>Horario de salida</label>
                <select class="ui fluid dropdown" name="departure_id">
                    <option value="{{ $reservation->departure->id }}">{{ $reservation->departure->hour }}</option>
                </select>
            </div>
            <div class="field">
                <label>Método de Pago</label>
                <input type="text" name="payment_method" id="payment_method" {{ $reservation->method->name }}>
            </div>
            @if ($reservation->card)
                <div class="field field-card">
                    <label>Últimos dígitos de la tarjeta</label>
                    <input type="number" name="card" id="card" placeholder="Últimos dígitos de la tarjeta" value="{{ $reservation->card }}">
                </div>
            @endif
            @if ($reservation->citypass)
                <div class="field field-citypass">
                    <label>TOTAL PASS</label>
                    <input type="text" name="citypass" id="citypass" placeholder="Zacatecas Total Pass" value="{{ $reservation->citypass }}">
                </div>
            @endif
            <div class="field">
                <label>Total</label>
                <input type="number" name="total" id="total" value="{{ $reservation->total }}">
            </div>
            <div class="three fields">
                <div class="field">
                    <label>Comisión Adultos</label>
                    <input type="number" name="commission" id="commission" value="{{ $reservation->commission_adults ? $reservation->commission_adults : 0 }}">
                </div>
                <div class="field">
                    <label>Comisión Adultos</label>
                    <input type="number" name="commission" id="commission" value="{{ $reservation->commission_kids ? $reservation->commission_kids : 0 }}">
                </div>
                <div class="field">
                    <label>Comisión INSEN</label>
                    <input type="number" name="commission" id="commission" value="{{ $reservation->commission_elders ? $reservation->commission_elders : 0 }}">
                </div>
            </div>
            <div class="field">
                <label>Notas extra</label>
                <textarea rows="2" name="notes">{{ $reservation->comments }}</textarea>
            </div>
            {{-- <button class="ui button green" id="validate" type="submit">Crear Reserva</button> --}}
        </form>
    </div>
    <div class="spacer" style="margin: 35px 0;"></div>
</x-app-layout>
