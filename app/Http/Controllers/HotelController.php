<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Zone;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hotels.index', [
            'hotels' => Hotel::orderBy('name')->paginate(20),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hotels.create', [
            'zones' => Zone::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|min:3',
            'key' => 'required|min:2|unique:hotels',
            'zone_id' => 'required'
        ]);

        Hotel::create($validated);

        return redirect()
                ->route('hotels.index')
                ->with('green','¡Hotel añadido!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        return view('hotels.edit', [
            'hotel' => $hotel,
            'zones' => Zone::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        $validated = $request->validate([
            'name' => 'required|min:3',
            'zone_id' => 'required'
        ]);

        $hotel->name = $request->name;
        $hotel->zone_id = $request->zone_id;
        $hotel->save();

        return redirect()
                ->route('hotels.index')
                ->with('yellow','¡Hotel actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $hotel->delete();

        return redirect()
                ->route('hotels.index')
                ->with('red','¡Hotel borrado!');
    }
}
