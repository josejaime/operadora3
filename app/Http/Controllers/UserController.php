<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use App\Models\Hotel;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index', [
            'users' => User::paginate(25),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create', [
            'companies' => Company::all(),
            'hotels' => Hotel::orderBy('name')->get(),
            'roles' => Role::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request;
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'role_id' => 'required',
            'hotel_id' => 'required',
            'company_id' => 'required',
            'password' => 'required|string|confirmed|min:8',
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'hotel_id' => $request->hotel_id == "null" ? null : $request->hotel_id,
            'role_id' => $request->role_id,
            'company_id' => $request->company_id,
            'username' => $request->username,
            'active' => $request->has('active') ? true : false,
            'password' => Hash::make($request->password),
        ]);

        return redirect()
                ->route('users.index')
                ->with('green', '¡Usuario creado con éxito!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', [
            'user' => $user,
            'roles' => Role::all(),
            'companies' => Company::all(),
            'hotels' => Hotel::orderBy('name')->get(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function reservations()
    {
        $reservations = Auth::user()->reservations()->paginate(50);

        return view('users.reservations', [
            'reservations' => $reservations,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function profile(User $user)
    {
        if( $user->id != Auth::user()->id ) {
            //abort(401);
            return redirect()
                    ->route('dashboard')
                    ->with('red','¡No tienes permiso para acceder!');
        }

        return view('users.profile', [
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|min:4',
            'hotel_id' => 'required',
            'role_id' => 'required',
            'company_id' => 'required',
        ]);

        $user->active = $request->has('active') ? true : false;
        $user->name = $request->name;
        $user->hotel_id = $request->hotel_id != -1 ? $request->hotel_id : null;
        $user->role_id = $request->role_id;
        $user->company_id = $request->company_id;
        $user->save();

        return redirect()
                ->route('users.index', ['user' => $user])
                ->with('yellow', '¡Perfil actulizado!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request, User $user)
    {
        //TODO: AUTH USER MUST BE THE USER
        $request->validate([
            'name' => 'required|min:4',
        ]);

        $user->name = $request->name;
        $user->save();

        return redirect()
                ->route('users.profile', ['user' => $user])
                ->with('status', '¡Perfil actulizado!')
                ->with('class', 'yellow');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if( $user->id == Auth::user()->id ) {
            return redirect()
                    ->route('users.index')
                    ->with('red', '¡No puedes borrar tu propio usuario!');
        }
        $user->delete();

        return redirect()
                ->route('users.index')
                ->with('red', '¡Usuario borrado!');
    }
}
