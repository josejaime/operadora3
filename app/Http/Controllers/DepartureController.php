<?php

namespace App\Http\Controllers;

use App\Models\Departure;
use App\Models\Tour;
use Illuminate\Http\Request;

class DepartureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tour = Tour::findOrFail($request->input('tour'));

        return view('departures.create', [
            'tour' => $tour,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request;
        $validated = $request->validate([
            'hour' => 'required',
            'tour_id' => 'required',
        ]);

        $departure = new Departure;
        $departure->hour = $request->hour;
        $departure->tour_id = $request->tour_id;

        if($request->has('type')) {
            $departure->type = $request->type;
        }

        //Departure::create($validated);
        $departure->save();

        $tour = Tour::findOrFail( $request->tour_id );

        return redirect()
                    ->route('tours.edit', ['tour' => $tour])
                    ->with('green', '¡Horario agregado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Departure  $departure
     * @return \Illuminate\Http\Response
     */
    public function show(Departure $departure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Departure  $departure
     * @return \Illuminate\Http\Response
     */
    public function edit(Departure $departure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Departure  $departure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Departure $departure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Departure  $departure
     * @return \Illuminate\Http\Response
     */
    public function close(Request $request, Departure $departure)
    {
        $departure = Departure::findOrFail( $request->input('departure') );
        $departure->closed = !$departure->closed;
        $departure->save();


        if( $departure->closed ) {
            return redirect()
                        ->route('tours.edit', ['tour' => $departure->tour])
                        ->with('red', '¡Horario cerrado!');
        }
        return redirect()
                    ->route('tours.edit', ['tour' => $departure->tour])
                    ->with('green', '¡Horario activado!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Departure  $departure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Departure $departure)
    {
        $tour = $departure->tour;
        $departure->delete();

        return redirect()
                    ->route('tours.edit', ['tour' => $tour])
                    ->with('red', '¡Horario borrado!');
    }
}
