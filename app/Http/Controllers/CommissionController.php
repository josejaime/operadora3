<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Models\Tour;
use App\Models\User;
use Illuminate\Http\Request;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        return view('commissions.create', [
            'tours' => Tour::all(),
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = Commission::where('tour_id', $request->tour_id)
                                ->where('user_id', $request->user_id)
                                ->get();

        if( count($check) > 0 ) {
            return redirect()
                    ->back()
                    ->with('red', 'Este usuario ya tiene asignadas comisiones para ese tour');
        }

        $request->validate([
            'kids' => 'required|numeric|min:0',
            'adults' => 'required|numeric|min:0',
            'elders' => 'required|numeric|min:0',
        ]);

        $commission = new Commission;

        $commission->user_id = $request->user_id;
        $commission->tour_id = $request->tour_id;
        $commission->kids = $request->kids ? $request->kids : 0;
        $commission->adults = $request->adults ? $request->adults : 0;
        $commission->elders = $request->elders ? $request->elders : 0;

        $commission->save();

        $user = User::findOrFail($request->user_id);

        return redirect()
                ->route('users.show', ['user' => $user])
                ->with('green','¡Comisión agregada!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function show(Commission $commission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function edit(Commission $commission)
    {
        return view('commissions.edit', [
            'commission' => $commission,
            'tours' => Tour::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commission $commission)
    {
        $request->validate([
            'kids' => 'required|numeric|min:0',
            'adults' => 'required|numeric|min:0',
            'elders' => 'required|numeric|min:0',
        ]);

        $commission->kids = $request->kids ? $request->kids : 0;
        $commission->adults = $request->adults ? $request->adults : 0;
        $commission->elders = $request->elders ? $request->elders : 0;

        $commission->save();

        //$user = User::findOrFail($commission->user_id);
        return redirect()
                ->route('users.show', ['user' => $commission->user])
                ->with('yellow','¡Comisión actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commission $commission)
    {
        $user = $commission->user;
        $commission->delete();

        return redirect()
                ->route('users.show', ['user' => $user])
                ->with('red','¡Comisión borrada!');
    }
}
