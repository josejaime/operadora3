<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\User;
use App\Models\Hotel;
use App\Models\Tour;
use App\Models\Method;
use App\Models\Departure;
use App\Models\Company;
use App\Models\Commission;
use Illuminate\Http\Request;
use PDF;
use Auth;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reservations.index');
    }

    /**
     * Show the form for select tour befeore anything.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectTour()
    {
        $maxi = Company::findOrFail(2);

        return view('reservations.select_tour', [
            'companies' => Company::all(),
            'fastes' => $maxi->tours->where('is_fast', true),
        ]);
    }

    /**
     * Show the form for select tour befeore
     * review its reservations.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectTourBeforeReview()
    {
        return view('reservations.select_tour_review', [
            'companies' => Company::all(),
        ]);
    }

    /**
     * Show the reservations from a
     * departure.
     *
     * @return \Illuminate\Http\Response
     */
    public function reviewTour(Request $request)
    {
        $departure = Departure::findOrFail( $request->input('departure_id') );
        $tour = $departure->tour;
        $reservations = Reservation::where('date', $request->input('date'))->where('departure_id', $request->input('departure_id') )->get();
        // return Reservation::all();

        if ($request->has('pdf')) {
            $pdf = PDF::loadView('pdfs.pdf_test', [
                'reservations' => $reservations,
                'departure' => $departure,
                'tour' => $tour,
                'date' => $request->input('date'),
            ]);
            //$pdf->loadView('pdfs.pdf_test');
            return $pdf->stream();
        }

        return view('reservations.tour_reservations',[
            'reservations' => $reservations,
            'departure' => $departure,
            'tour' => $tour,
            'date' => $request->input('date'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tour = Tour::findOrFail( $request->input('tour_id') );
        $date = $request->input('date');

        return view('reservations.create', [
            'hotels' => Hotel::all(),
            'methods' => Method::all(),
            'tour' => $tour,
            'date' => $date,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'first_name' => ['required', 'min:3'],
            'last_name' => ['required', 'min:2'],
            'telephone' => ['required'],
            'hotel_id' => ['required'],
            'room' => ['required', 'numeric'],
            'date' => ['required'],
            'departure_id' => ['required'],
            // 'kids' => ['required', 'numeric'],
            // 'adults' => ['required', 'numeric'],
            // 'insen' => ['required', 'numeric'],
            'actual_pay' => ['required', 'numeric'],
            'method_id' => ['required'],
        ]);
        //return $request->all();
        $departure = Departure::findOrFail( $request->departure_id );
        $method = Method::findOrFail( $request->method_id );
        $reservation = new Reservation();
        //==========================================
        //== Foreigns IDs
        //==========================================
        $reservation->user_id = Auth::user()->id;
        $reservation->hotel_id = $request->hotel_id;
        $reservation->tour_id = $departure->tour->id;
        $reservation->departure_id = $departure->id;
        //==========================================
        //== INFO
        //==========================================
        $reservation->client = $request->first_name.' '.$request->last_name;
        $reservation->email = $request->email;
        $reservation->telephone = $request->telephone;
        $reservation->room = $request->room;
        $reservation->date = $request->date;
        //==========================================
        //== NUMBERS
        //==========================================
        $reservation->adults = $request->adults ? $request->adults : 0;
        $reservation->kids = $request->kids ? $request->kids : 0;
        $reservation->elders = $request->insen ? $request->insen : 0;
        //==========================================
        //== PRICES
        //==========================================
        $reservation->price_adults = $request->cost_adults;
        $reservation->price_kids = $request->cost_kids;
        $reservation->price_elders = $request->cost_elders;

        //==========================================
        //== PAYMENT METHOD
        //==========================================
        $reservation->payment_method = $method->name;
        $reservation->method_id = $method->id;

        if ( $request->payment_method == "citypass") {
            $reservation->citypass = $request->citypass;
        } else if ( $request->payment_method == "tarjeta") {
            $reservation->card = $request->card;
        }
        //==========================================
        //== MAKE THE FOLIO WITH THE LAST ID
        //==========================================
        $last = Reservation::latest('id')->first();
        $id = 1;
        if ($last) {
            $id = $last->id + 1;
        }
        $company = $departure->tour->company->name;
        $folio = strtoupper(substr($company,0,3)).''.str_pad($id,7,'0',STR_PAD_LEFT);
        $reservation->folio = $folio;

        //==========================================
        //== TOTALS
        //==========================================
        $reservation->total = $request->total;
        $reservation->actual_pay = $request->actual_pay;
        $reservation->first_payment = $request->actual_pay;
        $reservation->comments = $request->comments;
        $reservation->remainig = (doubleval($reservation->total) - doubleval($reservation->actual_pay));

        $result = Auth::user()->commissions()->where('tour_id', $reservation->tour_id)->first();

        if( ! $result ) {
            $commissions = new Commission;
            $commissions->kids = 0;
            $commissions->adults = 0;
            $commissions->elders = 0;
        }
        else {
            $commissions = $result;
        }
        $reservation->commission_kids = $reservation->kids * $commissions->kids;
        $reservation->commission_adults = $reservation->adults * $commissions->adults;
        $reservation->commission_elders = $reservation->elders * $commissions->elders;
        $reservation->total_commission = $reservation->commission_kids + $reservation->commission_adults + $reservation->commission_elders;

        $reservation->save();

        return redirect()->route('reservations.tour.review', [
            'departure_id' => $reservation->departure_id,
            'date' => $reservation->date,
            'another' => true,
        ])->with('green', 'Reserva creada con éxito')->with('another', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function selectSeats( Request $request )
    {
        $departure = Departure::findOrFail( $request->departure_id );
        $tour = $departure->tour;
        $hotel = Hotel::findOrFail( $request->hotel_id );
        $method = Method::findOrFail( $request->method_id );

        return view('reservations.select_seats', [
            'reservation' => $request,
            'departure' => $departure,
            'tour' => $tour,
            'date' => $request->date,
            'hotel' => $hotel,
            'method' => $method,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        return view('reservations.show', [
            'reservation' => $reservation,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        $reservation->delete();
        return redirect()
                    ->route('dashboard')
                    ->with('red', 'Reserva borrada con éxito');
    }
}
