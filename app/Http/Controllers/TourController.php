<?php

namespace App\Http\Controllers;

use App\Models\Tour;
use App\Models\Company;
use Illuminate\Http\Request;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tours.index', [
            'tours' => Tour::paginate(20),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tours.create', [
            'companies' => Company::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|min:4',
            'cost_kids' => 'required|numeric|min:0',
            'cost_adults' => 'required|numeric|min:0',
            'cost_elders' => 'required|numeric|min:0',
            'cost_elders' => 'required|numeric|min:0',
            'company_id' => 'required',
        ]);

        Tour::create($request->all());

        return redirect()
                ->route('tours.index')
                ->with('green', '¡Tour agregado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function show(Tour $tour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour $tour)
    {
        return view('tours.edit', [
            'tour' => $tour,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour $tour)
    {
        $validated = $request->validate([
            'name' => 'required|min:4',
            'cost_kids' => 'required|numeric|min:0',
            'cost_adults' => 'required|numeric|min:0',
            'cost_elders' => 'required|numeric|min:0',
        ]);

        $tour->name = $request->name;
        $tour->cost_kids = $request->cost_kids;
        $tour->cost_adults = $request->cost_adults;
        $tour->cost_elders = $request->cost_elders;
        $tour->active = $request->has('active') ? true : false;
        $tour->is_fast = $request->has('is_fast') ? true : false;
        $tour->limit = $request->limit;

        $tour->save();

        return redirect()
                ->route('tours.index')
                ->with('yellow', '¡Tour actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour $tour)
    {
        $tour->delete();

        return redirect()
                ->route('tours.index')
                ->with('red', '¡Tour borrado!');
    }
}
