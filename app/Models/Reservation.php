<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    function departure() {
        return $this->belongsTo(Departure::class);
    }

    function hotel() {
        return $this->belongsTo(Hotel::class);
    }

    function method()
    {
        return $this->belongsTo(Method::class);
    }

    function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    public function getTour()
    {
        return $this->departure->tour;
    }
}
