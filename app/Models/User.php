<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'company_id',
        'hotel_id',
        'role_id',
        'active',
        'company_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    function role() {
        return $this->belongsTo(Role::class);
    }

    function hotel() {
        return $this->belongsTo(Hotel::class);
    }

    function commissions() {
        return $this->hasMany(Commission::class);
    }

    function company()
    {
        return $this->belongsTo(Company::class);
    }
}
