<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'key',
        'zone_id',
    ];

    function zone() {
        return $this->belongsTo(Zone::class);
    }

    function users()
    {
        return $this->hasMany(User::class);
    }
}
