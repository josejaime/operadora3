<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'cost_kids',
        'cost_adults',
        'cost_elders',
        'limit',
        'active',
        'company_id',
    ];

    function departures() {
        return $this->hasMany(Departure::class);
    }

    function hasEnabledDepartures() {
        $count = Departure::where('tour_id', $this->id)->where('closed',false)->get();

        return count($count) > 0 ? true : false;
    }

    function company()
    {
        return $this->belongsTo(Company::class);
    }
}
